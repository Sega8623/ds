Docker
https://docs.docker.com/engine/install/ (Установка)
 
Сначала платформа Docker проверила, присутствует ли на вашей локальной машине образ busybox:latest. Он отсутствовал, поэтому Docker извлекла его из хранилища Docker Hub в http://docker.io. После скачивания образа на вашу машину Docker создала из этого образа контейнер и запустила в нем команду. Команда echo напечатала текст в STDOUT, а затем процесс завершился, и контейнер был остановлен.
 
Создание тривиального приложения Node.js Теперь, когда у вас есть работающая настройка платформы Docker, вы создадите приложение. Вы построите тривиальное веб-приложение Node.js и упакуете его в образ контейнера. Приложение будет принимать HTTP-запросы и отвечать хостнеймом машины, на которой оно выполняется.
 
То, что этот код делает, должно быть понятно. Он запускает HTTP-сервер на порту 8080. На каждый запрос сервер откликается кодом состояния HTTP 200 OK и текстом "You've hit ". Обработчик запросов также записывает клиентский IP-адрес в стандартный вывод, который понадобится позже.

Создание файла Dockerfile для образа
 
Строка FROM определяет образ контейнера, который вы будете использовать в качестве отправной точки (базовый образ, поверх которого вы будете надстраивать). В вашем случае вы используете контейнерный образ node, тег 7. Во второй строке вы добавляете файл app.js из вашего локального каталога в корневой каталог образа под тем же именем (app.js). Наконец, в третьей строке вы определяете, какая команда должна быть выполнена, когда кто-то запускает образ. В вашем случае командой является node app.js.

Создание контейнерого образа
Теперь, когда у вас есть файл Dockerfile и файл app.js, у вас есть все необходимое для создания образа. Для того чтобы собрать его, выполните следующую ниже команду Docker:
 
Вы поручаете Docker создать образ с именем kubia на основе содержимого текущего каталога (обратите внимание на точку в конце команды build). Docker будет искать файл Dockerfile в каталоге и строить образ на основе инструкций в этом файле.
 

Для того чтобы скачать kubectl или minikube для Linux или Windows, замените darwin в URL-адресе на linux или windows.

Minikube – это инструмент, который настраивает одноузловой кластер.
Установка инструмента minikube
Для того чтобы установить его, для начала лучшего всего перейти в репозиторий Minikube на GitHub (http://github.com/kubernetes/minikube) и следовать приведенным там инструкциям.
 
Запуск кластера kubernetes с помощью minikube
 
Установка клиента kubernetes (kubectl)
Для взаимодействия с Kubernetes также необходим клиент CLI kubectl. Опять-таки от вас только требуется скачать его и положить на свой домашний путь.
 
Проверка работы кластера и обмена информацией с kubectl
 
Создание простого дескриптора YAML для модуля
Вам следует создать файл под названием kubia-manual.yaml (вы можете создать его в любом каталоге, в котором захотите)
 
Использование команды kubectl create для создания модуля
Для того чтобы создать модуль из файла YAML, используйте команду kubectl create:
 
Просмотр вновь созданного модуля в списке модулей
kubectl get pods





Использование образа docker
Запустите эту команду, чтобы запустить приложение в docker:

$ docker run -d --name telegram-alert-bot \
      -p 8080:8080 \
      vientoprojects/kubernetes-monitoring-telegram-bot
Использование Kubernetes (пользовательская конфигурация)
Вы можете использовать следующий пример для запуска приложения (вам нужно тайно изменить токен telegram-бота):

apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: telegram-alert-bot-secret
data:
  telegramToken: <telegram-bot-token-base64>
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: telegram-alert-bot-deployment
  labels:
    app: telegram-alert-bot
spec:
  replicas: 1
  selector:
    matchLabels:
      app: telegram-alert-bot
  template:
    metadata:
      labels:
        app: telegram-alert-bot
    spec:
      containers:
      - name: telegram-alert-bot
        image: docker.io/vientoprojects/kubernetes-monitoring-telegram-bot
        imagePullPolicy: Always
        ports:
        - containerPort: 8080
        env:
        - name: TELEGRAM_BOT_TOKEN
          valueFrom:
            secretKeyRef:
              name: telegram-alert-bot-secret
              key: telegramToken
---
apiVersion: v1
kind: Service
metadata:
  name: telegram-alert-bot-service
spec:
  selector:
    app: telegram-alert-bot
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 8080

https://cloud.yandex.ru/docs/managed-kubernetes/tutorials/marketplace/argo-cd

