redos
Переводим selinux в режим “предупреждать, но не запрещать”:

sudo vi /etc/selinux/config
Нам нужно переключить параметр SELINUX в режим permissive:

SELINUX=DISABLED

Сохраняем изменения в текстовом редакторе vi:

ESC
:wq!
Перезагружаем сервер.

ВАЖНО!

Для того, чтобы при установке избежать появления ошибки "CA configuration failed" выполните следующие действия.Убедитесь, что используется Java OpenJDK версии 8:

java -version
Если используется Java 11, то необходимо переключиться на 8-ую версию. Для этого выполните команды:

update-alternatives --config java
update-alternatives --config jre_openjdk
в каждой выберите нужную версию, указав ее порядковый номер. После чего нажмите Enter.

Удалите пакет:

dnf remove java-11-openjdk-headless
Устанавливаем пакеты для сервера FreeIPA:

sudo dnf install ipa-server ipa-server-dns


Дожидаемся окончания процесса установки пакетов.

Вот теперь мы можем запустить установку сервера FreeIPA со встроенный сервером DNS:

sudo ipa-server-install

В случае если не устанавливается  ipa-server-install --uninstall -U и переустановка

firewall-cmd --permanent --add-service= нужные порты

ipa user-add user2 и тд
Устанавливаем пакет для клиента FreeIPA:

apt install freeipa-client
Теперь все готово для того, чтобы присоединить нашего клиента к домену FreeIPA:

ipa-client-install --mkhomedir --domain itproblog.ru --realm ITPROBLOG.RU --enable-dns-updates

Установка необходимых пакетов на сервере SAMBA
перед настройкой обязательно добавьте всех samba пользователей
smbpasswd -a %username%
smbpasswd -e %username%
dnf install ipa-client sssd-libwbclient samba samba-client ipa-client-samba
Отключите службу:

systemctl disable --now systemd-timesyncd

Настройка серверов IPA и Samba
Настройка серверов IPA и Samba
Для правильного распределения DNS выполните следующие команды на сервере IPA:


kinit admin
ipa dnsrecord-add redosipa.ru --a-rec=samba --a-ip-address=ip адрес

Запустите настройку домена для обработки классов и атрибутов объектов, специфичных для Samba:
ipa-adtrust-install --add-sids
reboot

На сервере samba выполните ipa-client-samba, который создаст cifs и отредактирует /etc/samba/smb.conf для работы samba с ipa:
ipa-client-samba

Отредактируйте в файле /etc/samba/smb.conf значение netbios name, указав рабочую группу домена, в данном случае smb.conf будет выглядеть следующим образом:
...
realm = ДОМЕН
netbios name = ДОМЕН
workgroup = ДОМЕН
...

[global]
max smbd processes = 1000
dedicated keytab file = FILE:/etc/samba/samba.keytab
kerberos method = dedicated keytab
log file = /var/log/samba/log.%m
log level = 1
server role = member server
realm = ДОМЕН
netbios name = ДОМЕН
workgroup = ДОМЕН
idmap config * : range = 0 - 0
idmap config * : backend = tdb
idmap config ДОМЕН : range = 14000000 - 14199999
idmap config ДОМЕН : backend = sss
security = ads
printing = cups
printcap name = cups
load printers = yes
cups options = raw
[exchange]
path = /opt/exchange
read only = no
comment = exchange
browseable = yes
inherit acls = Yes
writable = yes
create mask = 1664
directory mask = 1775

[homes]
path = /opt/homs
read only = no
browseable = yes
writable = yes
create mask = 1777
directory mask = 1777
Создайте структуру ролей и привилегий для samba. Действия выполняются на контроллере домена ipa:
ipa permission-add "CIFS" --attrs={ipaNTHash,ipaNTSecurityIdentifier} --type=user --right={read,search,compare} --bindtype=permission
ipa privilege-add "CIFS server privilege"
ipa privilege-add-permission "CIFS server privilege" --permission="CIFS server can read user passwords"
ipa role-add "CIFS server"
ipa role-add-privilege "CIFS server" --privilege="CIFS server privilege"
ipa role-add-member "CIFS server" --services=cifs/samba.redosipa.ru
На samba-сервере внесите изменения в /etc/krb5.conf:
sed -i 's;default_ccache_name = KEYRING:persistent:%{uid};default_ccache_name = FILE:/tmp/krb5cc_%{uid};g' /etc/krb5.conf
Получите тикет и создайте keytab:
kinit admin
ipa-getkeytab -s ipaserver.test.local -p cifs/smb.test.local -k /etc/samba/samba.keytab
reboot

Настройка сетевого каталога на сервере Samba

На samba-сервере создайте каталог:

mkdir -p /opt/exchange/
Назначьте права на созданный каталог:
chown -R :users /opt/exchange
chmod -R 777 /opt/exchange
setfacl -m default:user:"admin":rwx /opt/exchange
setfacl -R -m default:user:"admin":rwx /opt/exchange
На контроллере домена IPA получите SID:

net getdomainsid
На файловом сервере samba настройте идентификатор безопасности (введите SID, полученный ранее):

net setdomainsid <SID>
На сервере samba выполните:

net -s groupmap add sid=S-1-5-32-546 unixgroup=nobody type=builtin
Добавьте в автозапуск службы samba и winbind, перезагрузите сервер samba:

systemctl enable smb winbind --now
reboot
Для проверки доступности сетевого каталога на сервере samba необходимо получить тикет (если он отсутствует) и выполнить команду подключения к сетевому каталогу:

kinit admin
smbclient -L -N --kerberos=required
Откройте на сервере samba файл /etc/exports в текстовом редакторе с привилегиями root.
/путь к директории *(rw,sync,no_root_squash,no_subtree_check)

Создайте файл с учетными данными пользователя Samba, который будет использоваться для автоматического монтирования. Создайте файл с именем, например, smbcredentials:

sudo nano /etc/smbcredentials

username=<Samba_Username>
password=<Samba_Password>
Замените <Samba_Username> и <Samba_Password> на реальные учетные данные пользователя Samba.
Установите права доступа к файлу smbcredentials, чтобы только суперпользователь мог прочитать его:

chmod 600 /etc/smbcredentials

Откройте файл /etc/auto.master в текстовом редакторе:
arduino
Копировать код
sudo nano /etc/auto.master
Добавьте следующую строку в конец файла auto.master:
удар
Копировать код
/mnt/samba /etc/auto.samba --ghost

Здесь /mnt/samba - это точка монтирования для сетевой папки Samba, а /etc/auto.samba - это путь к файлу, который будет содержать информацию о монтировании.

Создайте файл /etc/auto.samba:
sudo nano /etc/auto.samba
Внутри файла auto.samba добавьте следующую строку для каждого пользователя, которому необходимо монтирование сетевой папки Samba:
<Username> -fstype=nfs,rw,credentials=/etc/smbcredentials ://<Samba_Server>/<Samba_Share>
ЗДЕСЬ СОЗДАЕМ ВСЕХ ПОЛЬЗОВАТЕЛЕЙ
Замените <Username> на имя пользователя FreeIPA, <Samba_Server> на IP-адрес или DNS-имя сервера Samba, <Samba_Share> на имя общего ресурса Samba.

Пример содержимого файла auto.samba:

user1 -fstype=nfs,rw,credentials=/etc/smbcredentials ://<Samba_Server>/<Samba_Share>
user2 -fstype=nfs,rw,credentials=/etc/smbcredentials
://<Samba_Server>/<Samba_Share>
Сохраните изменения в файле auto.samba и закройте редактор.
Перезапустите службу autofs:
sudo systemctl restart autofs

Теперь при обращении к директории /mnt/samba/<Username> автоматически будет происходить монтирование сетевой папки Samba для соответствующего пользователя FreeIPA, указанного в файле auto.samba.

ДЛЯ ПОДКЛЮЧЕНИЯ СЕТЕВЫХ ДОМАШНИХ ПАПОК СИПОЛЬЗУЕМ СЛЕДУЮЩУЮ КОНСТРУКЦИЮ

сетевая папка уже создана на samba сервере


выбираем пользователя
опускаемся в самый низ
и указываем наши параметры

далее сохраняем



Настройка квот
sudo setquota -u sammy 200M 220M 0 0 /

для всех пользователей

Приведенная выше команда удвоит ограничения квоты на основе блоков sammyдо 200 мегабайт и 220 мегабайт. 0 0Для программных и жестких ограничений на основе индексных индексов указывает, что они остаются неустановленными. Это необходимо, даже если мы не устанавливаем никаких квот на основе индексов.

Еще раз, используйте quotaкоманду, чтобы проверить нашу работу:

sudo quota -vs sammy

Output
Disk quotas for user sammy (uid 1000):
Filesystem space quota limit grace files quota limit grace
/dev/vda1 40K 200M 220M 13 0 0

создание диска
$ dd if=/dev/zero of=filename bs=1024 count=2M
$ sudo mkfs.ext4 filename
$ cat /etc/fstab
/path/to/filename /mount/point ext4 defaults,users
и создание второго диска
$ dd if=/dev/zero1 of=filename bs=2048 count=2M
$ sudo mkfs.ext4 filename
$ cat /etc/fstab
/path/to/filename /mount/point ext4 defaults,users





vyos
Переход в превилигированный режим
vyos@vyos:~$ '''configure'''

Просмотр подсказок точно такой же как в cisco


VLAN Sub-Interfaces (802.1Q)
'''set interfaces ethernet <name> vif <vlan-id>'''.
'''set interfaces ethernet eth1 vif 100 address '192.168.100.1/24''''
настройка DHCP и DNS forwarders

Ethernet Interfaces
Настройка ip адреса
'''set interfaces ethernet eth1 address '192.168.0.1/2'""

""set interfaces ethernet eth1 duplex 'auto'''''''

""set interfaces ethernet eth1 speed 'auto''''

Сохранение конфигурации
vyos@vyos# '''save'''
win server
pdc синхронизация

tzutil /l настройка часового пояса

веб-сервер
sudo apt update
sudo apt install apache2 apache2-utils
sudo a2enmod auth_basic
sudo systemctl restart apache2
Создайте файл, который будет содержать информацию о пользователях и их паролях. Вы можете создать его с помощью команды htpasswd, указав имя файла и имя пользователя:
sudo htpasswd -c /etc/apache2/.htpasswd %username%

настройка по группам

Шаг 1: Создайте файл группы Создайте файл, который будет содержать информацию о группах и пользователях, принадлежащих к этим группам. Файл обычно имеет расширение .group или .htgroup. В каждой строке указывается имя группы, за которым следуют имена пользователей, разделенные пробелами или запятыми. Например:

mygroup: user1 user2 user3
Шаг 2: Создайте файл паролей

Создайте файл паролей (
.htpasswd), который содержит информацию о пользователях и их паролях. Используйте htpasswd, чтобы создать файл паролей.

/etc/apache2/sites-available/

<VirtualHost *:80>
    ServerName example.com
    Redirect permanent / https://example.com/
</VirtualHost>

<VirtualHost *:443>
    ServerName example.com

    SSLEngine on
    SSLCertificateFile /path/to/certificate.crt
    SSLCertificateKeyFile /path/to/private.key
    SSLCertificateChainFile /path/to/ca_bundle.crt

   <Directory /path/to/directory>
    Options -Indexes -FollowSymLinks
    AllowOverride All
    Require valid-user
    AuthType Basic
    AuthName "Restricted Area"
    AuthUserFile /etc/apache2/.htpasswd
    AuthGroupFile /path/to/group/file
    Require group mygroup
    Require all denied
    Require ip 192.168.0.0/24 
</Directory>

RewriteEngine On
RewriteCond %{HTTP_HOST} ^(?:[0-9] {1,3}\.){3} [0-9] {1,3}$
RewriteRule ^ - [R=404,L]

</VirtualHost>




apache
политика на изменения файлов ставится в файле sites-enabled

Rewrite cond вычисление по ip адресу подключение и перенаправляет на 404 ошибку

выполнен через rsync



перемещаемые профили


в samba соответственно прописываем следующий конфиг

smbpasswd -a добавление пользователей
smbpasswd -e включение пользователей

в терминале указать на обе папки chmod 1777 /opt/docs и exchange

[users]
path = /opt/users
force user = nobody
browsable = yes
writable = yes
create mask = 0777
directory mask 0777


samba
smbpasswd -a добавление пользователей
smbpasswd -e включение пользователей

в терминале указать на обе папки chmod 1777 /opt/docs и exchange

[exchange]
path = /opt/exchange
force user = nobody
browsable = yes
writable = yes


[docs]
path = /opt/docs
force user = nobody
browsable = yes
readonly = yes
write list = @docadmin


workgroup - рабочая группа. Для упрощения работы пользователей WORKGROUP указывается, как группа по умолчанию. Если в вашей сети имя рабочей группы изменено, то следует изменить это значение и для Samba;
security - уровень безопасности сервера. Значение user означает авторизацию по паре логин/пароль;
map to guest - параметр определяет способ обработки запросов. Значение bad user означает, что запросы с неправильным паролем будут отклонены, даже если такое имя пользователя существует;
wins support - включить или выключить поддержку WINS;
dns proxy - возможность проксирования запросов к DNS.
Настройки директорий выполняются в соответствующих секциях:

path - полный путь до директории на жестком диске;

guest ok - возможность доступа к каталогу без пароля (гостевой);

browsable - показывать ли каталог (“шару”) на сервере среди прочих. Если параметр установлен как “no”, то доступ будет возможен по полному пути, например ip-addresshidden_directory;

force user - пользователь от которого ведется работа с каталогом. Для повышения безопасности сервера, обычно используют nobody. Главное, не использовать пользователя root - это небезопасно.

writable - установка значения как “yes” позволяет пользователю выполнять действия над файлами внутри каталога - переименование, добавление, удаление, перемещение в подкаталог и копирование;

valid users - список пользователей у которых есть доступ к каталогу. Если пользователей несколько, их имена указываются через запятую. Если необходим доступ для пользователей принадлежащих группе, перед именем группы устанавливается символ ”at” @ (“собака”).


Автоматическое монтирование ресурсов при входе пользователя с помощью pam_mount

Для автоматического монтирования разделяемых файловых ресурсов при входе пользователя используется pam модуль pam_mount, предоставляемый пакетом libpam-mount, который может быть установлен следующим образом:

apt-get install libpam-mount

Настройка pam модуля осуществляется с помощью конфигурационного файла
/etc/security/pam_mount.conf.xml
.

Использование pamмодуля указывается в соответствующих pamсценариях (common-auth, common-session) в каталоге /etc/pam.d.

Описание возможностей pamмодуля pam_mountи формат его конфигурационного файла приведены в руководстве manдля pam_mountи pam_mount.conf.

Для монтирования  с помощью pam-mount разделяемых файловых ресурсов СЗФС CIFS, представляющих собой домашние каталоги пользователей, конфигурационный файл должен быть модифицирован следующим образом (в пределах тега pam_mount):

/etc/security/pam_mount.conf.xml

<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE pam_mount SYSTEM "pam_mount.conf.xml.dtd">
 
<pam_mount>
 
<logout wait="500000" hup="1" term="1" kill="1" />
<mkmountpoint enable="1" remove="true" />
<cifsmount>mount.cifs //%(SERVER)/%(VOLUME) %(MNTPT) -o %(OPTIONS)</cifsmount>
<volume fstype="cifs" server="fileserver1.org.net"
path="homes" mountpoint="[mountpoint_homes]/%(USER)"
options="user=%(USER),rw,setuids,perm,soft,iocharset=utf8" />
 
</pam_mount>
Внимание
При использовании с аутентификацией Kerberosв ЕПП в строке опций монтирования должен быть указан параметр аутентификации sec=krb5i. В этом случае при монтировании будет использоваться текущий кэш Kerberosпользователя.

Так же в строке опций монтирования должен присутствовать параметр cruid=%(USERUID), поскольку монтирование во время создания сессии выполняется от имени привилегированного пользователя.

Для точки монтирования mountpointдолжен быть указан отдельный каталог, например: /media/ald_share

Пример:
path="share" mountpoint="/media/ald_share"
options="user=%(USER),rw,setuids,perm,soft,sec=krb5i,cruid=%(USERUID),iocharset=utf8" />
Тег logoutопределяет поведение в процессе размонтирования ФС. К этому времени все процессы должны освободить точку монтирования, в противном случае им посылаются соответствующие сигналы прерывания работы.

Тег mkmountpointотвечает за автоматическое создание и удаление точки монтирования.

Тег cifsmountопределяет команду, с помощью которой монтируется указанный тип ФС.

Тег volumeобъявляет непосредственно параметры монтирования разделяемого файлового ресурса. Пользователь должен существовать в БД учетных записей Sambaи иметь соответствующий пароль.
sudoers
Строки пользовательских привилегий

Четвертая строка, которая определяет для пользователя root привилегии sudo, отличается от предыдущих строк. Давайте посмотрим, что означают различные поля:

root ALL=(ALL:ALL) ALL Первое поле показывает имя пользователя, которое правило будет применять к (root).
root ALL=(ALL:ALL) ALL Первое “ALL” означает, что данное правило применяется ко всем хостам.
root ALL=(ALL:ALL) ALL Данное “ALL” означает, что пользователь root может запускать команды от лица всех пользователей.
root ALL=(ALL:ALL) ALL Данное “ALL” означает, что пользователь root может запускать команды от лица всех групп.
root ALL=(ALL:ALL) ALL Последнее “ALL” означает, что данные правила применяются всем командам.

root ALL=(ALL:ALL) ALL
ddns
dnssec-keygen -a HMAC-MD5 -b 128 -n USER DHCP_UPDATER
dhcpd.conf:
ddns-updates on;
update-static-leases on;
ddns-domainname "status.ks.ua";
ddns-update-style interim;
ignore client-updates;
update-static-leases true;
default-lease-time 3600;
max-lease-time 3600;
key DHCP_UPDATER:
algorithm HMAC-MD5;
secret X/Vl6yCJ9xz3UE+FDV7gNQ==;
local-address 10.1.1.1;
zone internal.status.ks.ua:
primary 10.1.1.1;
key DHCP_UPDATER;
zone 1.1.10.in-addr.arpa.:
primary 10.1.1.1;
key DHCP_UPDATER;
zone 2.1.10.in-addr.arpa.:
primary 10.1.1.1;
key DHCP_UPDATER;
Редактируем bind named.conf.local:
forwarders { 8.8.8.8; };
key DHCP_UPDATER:
algorithm HMAC-MD5.SIG-ALG.REG.INT;
secret "X/Vl6yCJ9xz3UE+FDV7gNQ==";
zone "internal.status.ks.ua":
type master;
file "int/internal.status.ks.ua";
allow-update { key DHCP_UPDATER; };
zone "1.1.10.in-addr.arpa":
type master;
file "int/10.1.1.rev";
allow-update { key DHCP_UPDATER; };
динамический dns
Откройте файл named.conf в редакторе конфигурации BIND9:
sudo nano /etc/bind/named.conf
Добавьте следующий код в файл named.conf, чтобы настроить зоны для динамического DNS:
// Определение зоны для динамического DNS
zone "example.com" {
type master;
file "/etc/bind/db.example.com";
allow-update { key "dns-key"; };
};

Замените "example.com" на имя вашей доменной зоны.

Создайте файл зоны /etc/bind/db.example.com (в примере зона названа example.com) и добавьте в него следующий код:
$TTL 86400 @ IN SOA ns1.example.com. root.example.com. (
2023052801 ; Serial
3600 ; Refresh
1800 ; Retry
604800 ; Expire
86400 ; Minimum TTL )
@ IN NS ns1.example.com. ;
Добавьте A-записи для основного и дополнительного DNS-сервера
ns1 IN A 192.168.0.1
ns2 IN A 192.168.0.2 ;
Задайте запись для динамического DNS
dyn IN A 192.168.0.3

Замените IP-адреса и имена серверов (ns1.example.com, ns2.example.com, dyn.example.com) на соответствующие ваши.

Создайте файл ключа для обновления динамического DNS. В терминале выполните команду:
sudo dnssec-keygen -a HMAC-MD5 -b 128 -n USER dns-key
Откройте файл /etc/bind/named.conf.local:
sudo nano /etc/bind/named.conf.local
Добавьте следующую строку в файл named.conf.local:
key "dns-key" {
algorithm HMAC-MD5.SIG-ALG.REG.INT;
secret "СКРЫТЫЙ_КЛЮЧ";
};

Замените "СКРЫТЫЙ_КЛЮЧ" на сгенерированный ключ, который был создан в предыдущем шаге.

Сохраните и закройте файлы named.conf и named.conf.local.
Перезапустите службу BIND9:
sudo service bind9 restart

Теперь ваш BIND9 DNS-сервер настроен для поддержки динамического DNS. Вы можете обновлять запись динамического DNS, используя ключ "dns-key" и с помощью команды nsupdate или соответствующей функциональности в клиентском приложении.

DHCP
Чтобы настроить ISC DHCP Server для динамического DNS (DDNS) с использованием BIND9 в качестве сервера DNS, выполните следующие шаги:

Установите пакеты isc-dhcp-server и dnsutils (если они еще не установлены):
sudo apt-get update sudo apt-get install isc-dhcp-server dnsutils
Откройте файл конфигурации DHCP-сервера /etc/dhcp/dhcpd.conf в редакторе:
sudo nano /etc/dhcp/dhcpd.conf
Добавьте следующие строки в файл dhcpd.conf, чтобы настроить DDNS:
# Включение поддержки DDNS
ddns-update-style interim;
ddns-updates on;
ignore client-updates;
# Определение ключа DDNS
key DDNS_UPDATE {
algorithm HMAC-MD5.SIG-ALG.REG.INT;
secret "СКРЫТЫЙ_КЛЮЧ"; };
# Определение зоны DDNS
zone example.com. {
primary 192.168.0.1;
key DDNS_UPDATE;
}
# Определение статического DNS-имени для DHCP-клиента
zone 0.168.192.in-addr.arpa. {
primary 192.168.0.1;
key DDNS_UPDATE; }
# Пример настройки пула DHCP
subnet 192.168.0.0 netmask 255.255.255.0 {
range 192.168.0.100 192.168.0.200;
option routers 192.168.0.1;
option domain-name-servers 192.168.0.1;
option domain-name "example.com";
option dhcp-lease-time 3600;
option dhcp-ddns-domainname "example.com.";
option dhcp-ddns-updates on;
option dhcp-ddns-update-style interim;
option dhcp-ddns-rev-domainname "in-addr.arpa.";
}

Замените "СКРЫТЫЙ_КЛЮЧ" на сгенерированный ключ для DDNS, который был создан на предыдущем шаге. Замените "example.com" на вашу доменную зону.

Сохраните и закройте файл dhcpd.conf.
Откройте файл конфигурации DHCP-сервера /etc/default/isc-dhcp-server в редакторе:
sudo nano /etc/default/isc-dhcp-server
Убедитесь, что переменная INTERFACESv4 содержит имя сетевого интерфейса, на котором должен работать DHCP-сервер. Например:
INTERFACESv4="eth0"
Сохраните и закройте файл isc-dhcp-server.
Перезапустите службу DHCP-сервера:
sudo service isc-dhcp-server restart

Теперь ваш ISC DHCP Server настроен для динамического DNS. Он будет автоматически обновлять записи DNS при назначении нового IP-адреса клиентам DHCP.
Для настройки маршрутизации через VPN в OpenVPN можно использовать следующие параметры:
Для настройки маршрутизации через VPN в OpenVPN можно использовать следующие параметры:

route: Определяет статический маршрут для клиента или сервера. Например:
Эта инструкция добавляет статический маршрут для сети 192.168.2.0/24 через VPN-туннель.
push "route ...": Как уже упоминалось, параметр `pushpush также может использоваться для передачи маршрутов от сервера к клиентам. Например:
Эта инструкция указывает клиенту добавить маршрут для сети 192.168.3.0/24 через VPN-туннель
redirect-gateway: Этот параметр перенаправляет весь сетевой трафик клиента через VPN-тунне
В результате, весь трафик, идущий с клиента, будет отправляться через VPN-сервер.
route-nopull: Этот параметр предотвращает перезапись маршрутов, полученных от сервера, и позволяет клиенту самостоятельно управлять своими маршрутами. Например:
Клиент не будет автоматически получать и применять маршруты, отправленные сервером, и сможет управлять маршрутизацией по своему усмотрению.

Эти параметры позволяют настраивать маршрутизацию через VPN в зависимости от ваших требований и сценария использования.

Команда route не вводится непосредственно в файле конфигурации OpenVPN (ни в клиентском, ни в серверном). Она используется в операционной системе для добавления, удаления или изменения маршрутов.

В файле конфигурации OpenVPN можно указать параметры маршрутизации, которые будут отправлены клиентам или серверу при установлении VPN-соединения. Например, параметр push "route ..." в серверном конфигурационном файле будет отправлять маршруты клиентам, а параметр route ... в клиентском конфигурационном файле задает статические маршруты, которые будут применяться на клиентской стороне.

Команды route используются вне файла конфигурации OpenVPN для настройки маршрутизации в операционной системе. Вы можете использовать команду route в командной строке операционной системы на клиенте или сервере, чтобы добавить, удалить или изменить маршруты, связанные с VPN-соединением.
rsync
файл crontab

* * * * *  rsync -avz /var/www/html/task.html rsync@ip-адрес удаленного узла::task
* * * * *  sleep 30; rsync -avz /var/www/html/task.html rsync@ip-адрес удаленного узла::task

файл /etc/rsyncd.conf


pid file = /var/run/rsyncd.pid
lock file = /var/run/rsync.lock
log file = /var/log/rsync.log
[share]
path = /var/www/html/
include = task.html
hosts allow = ip адрес удаленного сервера
hosts deny = *
uid = root
gid = root
read only = false



netplan
network:
ethernets:
ens160:
  addresses:
  -10.10.10.1/24
каждый enter показывает принадлежность следующей команды, также показывает приоритет
netplan apply

dns
где dnssec-enable — включение или отключение dnssec на уровне сервера; dnssec-validation — проверка корректность ответов; dnssec-lookaside — разрешение использовать сторонние корни DNSSEC.
dnssec - инструмент позволяющий защитить dns службу от хакерских атак основанных на сертификатах
Общие сведения

Named- это демон, входящий в состав пакета bind9и являющийся сервером доменных имен. Демон namedможет реализовывать функции серверов любого типа: master, slave, cache. На приведенной схеме я постарался максимально прозрачно отобразить основной принцип работы DNS сервера BIND. Бинарник, который выполняет основную работу, расположен в /usr/sbin/named. Он берет настройки из основного конфигурационного файла, который называется named.confи расположен в каталоге /etc/bind. В основном конфигеописывается рабочий каталог асервера, зачастую это каталог /var/cache/bind, в котором лежат файлы описания зони другие служебные файлы. Соответствиеназвания зоныи файла описания зонызадаетраздел zoneс параметром file. Раздел zoneтак же задает тип ответственности данного сервера за зону (master, slave и др.), а так же определяет особые параметры для текущей зоны (например, на каком интерфейсе обрабатывать запросы для текущей зоны). В файлах описания зонсодержатся параметры зон и записи ресурсов (пути, указанные в данном абзаце могут отличаться, это зависит от дистрибутива Linux или параметров сборки сервера из исходников).

Параметры (синтаксис) named.conf
Синтаксис файла named.conf придерживается следующих правил:

IP-адреса - список IP должен быть разделен символом ";" , возможно указывать подсеть в формате 192.168.1.1/24 или 192.168.1.1/255.255.255.0, (для исключения IP перед ним нужно поставить знак !), возможно указывать имена "any", "none", "localhost" в двойных кавычках.

Комментарии - строки начинающиеся на #, // и заключенные в /* и */ считаются комментариями.

В файлах описания зон - символ @ является "переменной" хранящей имя зоны, указанной в конфигурационном файле named.conf или в директиве @ $ORIGIN текущего описания зоны.

Каждая завершенная строка параметров должна завершаться символом ; .

Раздел Acl
Acl (access control list) - позволяет задать именованный список сетей. Формат раздела: acl "имя_сети" {ip; ip; ip; };

Раздел Options
Раздел Options задает глобальные параметры конфигурационного файла, управляющие всеми зонами. Данный раздел имеет формат: options {операторы_раздела_Options};. Options может быть "вложен" в раздел Zone, при этом он переопределяет глобальные параметры. Часто используемые операторы options:

allow-query {список_ip} - Разрешает ответы на запросы только из список_ip. При отсутствии - сервер отвечает на все запросы.
allow-recursion {список_ip} - На запросы из список_ip будут выполняться рекурсивные запросы. Для остальных - итеративные. Если не задан параметр, то сервер выполняет рекурсивные запросы для всех сетей.
allow-transfer {список_ip} - Указывает список серверов, которым разрешено брать зону с сервера (в основном тут указывают slave сервера)
directory /path/to/work/dir - указывает абсолютный путь к рабочему каталогу сервера. Этот оператор допустим только в разделе options.
forwarders {ip порт, ip порт...} - указывает адреса хостов и если нужно порты, куда переадресовывать запросы (обычно тут указываются DNS провайдеров ISP).
forward ONLY или forward FIRST - параметр first указывает, DNS-серверу пытаться разрешать имена с помощью DNS-серверов, указанных в параметре forwarders, и лишь в случае, если разрешить имя с помощью данных серверов не удалось, то будет осуществлять попытки разрешения имени самостоятельно.
notify YES|NO - YES - уведомлять slave сервера об изменениях в зоне, NO - не уведомлять.
recursion YES|NO - YES - выполнять рекурсивные запросы, если просит клиент, NO - не выполнять (только итеративные запросы). Если ответ найден в кэше, то возвращается из кэша. (может использоваться только в разделе Options)
Раздел Zone
Определяет описание зон(ы). Формат раздела: zone {операторы_раздела_zone}; Операторы, которые наиболее часто используются:

allow-update {список_ip} - указывает системы, которым разрешено динамически обновлять данную зону.
file "имя_файла" - указывает путь файла параметров зоны (должен быть расположен в каталоге, определенном в разделе options оператором directory)
masters {список_ip} -указывает список мастер-серверов. (допустим только в подчиненных зонах)
type "тип_зоны" - указывает тип зоны, описываемой в текущем разделе,тип_зоны может принимать следующие значения:
forward - указывает зону переадресации, которая переадресовывает запросы, пришедшие в эту зону.
hint - указывает вспомогательную зону (данный тип содержит информацию о корневых серверах, к которым сервер будет обращаться в случае невозможности найти ответ в кэше)
master - указывает работать в качестве мастер сервера для текущей зоны.
slave - указывает работать в качестве подчиненного сервера для текущей зоны.
Дополнительные параметры конфигурации
Значения времени в файлах зон по умолчанию указывается в секундах, если за ними не стоит одна из следующих букв: S - секунды, M - минуты, H- часы, D - дни, W - недели. Соответственно, запись 2h20m5s будет иметь значение 2 часа 20 минут 5 секунд и соответствовать 8405 секунд.

Любое имя хоста/записи, не оканчивающиеся точкой считается неFQDN именем и будет дополнено именем текущей зоны. Например, запись domen в файле зоны examle.com будет развернуто в FQDN-имя domen.examle.com. .

В конфигурационных файлах BIND могут применяться следующие директивы:

$TTL - определяет TTL по-умолчанию для всех записей в текущей зоне.
$ORIGIN - изменяет имя зоны с указанного в файле named.conf. При этом, область действия данной директивы не распространяется "выше" (то есть если файл включен директивой $INCLUDE, то область действия$ORIGN не распространяется на родительский)
$INCLUDE - включает указанный файл как часть файла зоны.
Установите dig на Ubuntu и Debian
sudo apt update && sudo apt install dnsutils

Понимание dig вывода
В простейшей форме, когда используется для запроса одного хоста (домена) без каких-либо дополнительных параметров, команда dig довольно многословна.

В следующем примере мы работаем в домене linux.org :

dig linux.org
Результат должен выглядеть примерно так:

Давайте разберемся по разделам и объясним вывод команды dig :

В первой строке вывода выводится установленная версия dig и запрашиваемое доменное имя. Вторая строка показывает глобальные параметры (по умолчанию только cmd).

Если вы не хотите, чтобы эти строки включались в вывод, используйте параметр +nocmd . Эта опция должна быть самой первой после команды dig .
В следующем разделе приведены технические сведения об ответе, полученном от запрашиваемого центра (DNS-сервера). В заголовке отображается код операции (действие, выполняемое командой dig ) и статус действия. В этом примере статус NOERROR , что означает, что запрошенный орган обслужил запрос без каких-либо проблем.

Этот раздел можно удалить с помощью параметра +nocomments , который также отключает заголовки некоторых других разделов.
Псевдо-раздел «OPT» отображается только в более новых версиях утилиты dig . Вы можете узнать больше о механизмах расширения для DNS (EDNS)здесь .

Чтобы исключить этот раздел из вывода, используйте параметр +noedns .
В разделе «ВОПРОС» dig показывает запрос (вопрос). По умолчанию dig запрашивает запись A.

Вы можете отключить этот раздел, используя опцию +noquestion .
Раздел «ОТВЕТ» дает нам ответ на наш вопрос. Как мы уже упоминали, по умолчанию dig запрашивает запись A. Здесь мы видим, что домен linux.org указывает на IP-адрес 104.18.59.123 .

Обычно вы не хотите отключать ответ, но вы можете удалить этот раздел из вывода, используя параметр +noanswer .
Раздел «АВТОРИТЕТ» сообщает нам, какие серверы являются полномочными для ответа на запросы DNS о запрошенном домене.

Вы можете отключить этот раздел вывода, используя параметр +noauthority .
Раздел «ДОПОЛНИТЕЛЬНО» предоставляет нам информацию об IP-адресах авторитетных DNS-серверов, указанных в разделе полномочий.

Параметр +noadditional отключает дополнительный раздел ответа.
Последний раздел вывода dig включает статистику запроса.

Вы можете отключить эту часть с +nostats опции +nostats .
Печать только ответа
Как правило, вы хотите получить только краткий ответ на свой запрос о dig .

1. Получите короткий ответ
Чтобы получить краткий ответ на свой вопрос, используйте опцию +short :

dig linux.org +short

104.18.59.123
104.18.58.123

Вывод будет включать только IP-адреса записи A.

2. Получите подробный ответ
Чтобы получить более подробный ответ, отключите все результаты с помощью параметров +noall , а затем включите только раздел ответов с параметром +answer .

dig linux.org +noall +answer

; <<>> DiG 9.13.3 <<>> linux.org +noall +answer
;; global options: +cmd
linux.org. 67 IN A 104.18.58.123
linux.org. 67 IN A 104.18.59.123

Сервер имен для конкретного запроса
По умолчанию, если сервер имен не указан, dig использует серверы, перечисленные в файле /etc/resolv.conf .

Чтобы указать сервер имен, для которого будет выполняться запрос, используйте символ @ (at), за которым следует IP-адрес или имя хоста сервера имен.

Например, чтобы запросить у сервера имен Google (8.8.8.8) информацию о домене linux.org вы должны использовать:

dig linux.org @8.8.8.8

; <<>> DiG 9.13.3 <<>> linux.org @8.8.8.8
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 39110
;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;linux.org. IN A

;; ANSWER SECTION:
linux.org. 299 IN A 104.18.58.123
linux.org. 299 IN A 104.18.59.123

;; Query time: 54 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Fri Oct 12 14:28:01 CEST 2018
;; MSG SIZE rcvd: 70

Запрос типа записи
Dig позволяет выполнять любой допустимый DNS-запрос, добавляя тип записи в конец запроса. В следующем разделе мы покажем вам примеры того, как искать наиболее распространенные записи, такие как A (IP-адрес), CNAME (каноническое имя), TXT (текстовая запись), MX (почтовый обменник) и NS ( серверов имен).

1. Запрос записей A
Чтобы получить список всех адресов для доменного имени, используйте параметр a :

dig +nocmd google.com a +noall +answer

google.com. 128 IN A 216.58.206.206

Как вы уже знаете, если тип записи DNS не указан, dig запросит запись A. Вы также можете запросить запись A, не указывая параметр a .

2. Запрос записей CNAME
Чтобы найти псевдоним домена, используйте опцию cname :

dig +nocmd mail.google.com cname +noall +answer

mail.google.com. 553482 IN CNAME googlemail.l.google.com.

3. Запрос записей TXT
Используйте параметр txt чтобы получить все записи TXT для определенного домена:

dig +nocmd google.com txt +noall +answer

google.com. 300 IN TXT "facebook-domain-verification=22rm551cu4k0ab0bxsw536tlds4h95"
google.com. 300 IN TXT "v=spf1 include:_spf.google.com ~all"
google.com. 300 IN TXT "docusign=05958488-4752-4ef2-95eb-aa7ba8a3bd0e"

4. Запрос записей MX.
Чтобы получить список всех почтовых серверов для определенного домена, используйте параметр mx :

dig +nocmd google.com mx +noall +answer

google.com. 494 IN MX 30 alt2.aspmx.l.google.com.
google.com. 494 IN MX 10 aspmx.l.google.com.
google.com. 494 IN MX 40 alt3.aspmx.l.google.com.
google.com. 494 IN MX 50 alt4.aspmx.l.google.com.
google.com. 494 IN MX 20 alt1.aspmx.l.google.com.

5. Запрос NS-записей
Чтобы найти авторитетные серверы имен для нашего конкретного домена, используйте параметр ns :

dig +nocmd google.com ns +noall +answer

google.com. 84527 IN NS ns1.google.com.
google.com. 84527 IN NS ns2.google.com.
google.com. 84527 IN NS ns4.google.com.
google.com. 84527 IN NS ns3.google.com.

6. Запрос всех записей
Используйте параметр any чтобы получить список всех записей DNS для определенного домена:

dig +nocmd google.com any +noall +answer

google.com. 299 IN A 216.58.212.14
google.com. 299 IN AAAA 2a00:1450:4017:804::200e
google.com. 21599 IN NS ns2.google.com.
google.com. 21599 IN NS ns1.google.com.
google.com. 599 IN MX 30 alt2.aspmx.l.google.com.
google.com. 21599 IN NS ns4.google.com.
google.com. 599 IN MX 50 alt4.aspmx.l.google.com.
google.com. 599 IN MX 20 alt1.aspmx.l.google.com.
google.com. 299 IN TXT "docusign=05958488-4752-4ef2-95eb-aa7ba8a3bd0e"
google.com. 21599 IN CAA 0 issue "pki.goog"
google.com. 599 IN MX 40 alt3.aspmx.l.google.com.
google.com. 3599 IN TXT "facebook-domain-verification=22rm551cu4k0ab0bxsw536tlds4h95"
google.com. 21599 IN NS ns3.google.com.
google.com. 599 IN MX 10 aspmx.l.google.com.
google.com. 3599 IN TXT "v=spf1 include:_spf.google.com ~all"
google.com. 59 IN SOA ns1.google.com. dns-admin.google.com. 216967258 900 900 1800 60

Обратный поиск в DNS
Чтобы запросить имя хоста, связанное с определенным IP-адресом, используйте параметр -x .

Например, чтобы выполнить обратный поиск по 208.118.235.148 , введите:

dig -x 208.118.235.148 +noall +answer
Как видно из выходных данных ниже, IP-адрес 208.118.235.148 связан с именем хоста wildebeest.gnu.org .

; <<>> DiG 9.13.3 <<>> -x 208.118.235.148 +noall +answer
;; global options: +cmd
148.235.118.208.in-addr.arpa. 245 IN PTR wildebeest.gnu.org.

Массовые запросы
Если вы хотите запросить большое количество доменов, вы можете добавить их в файл (по одному домену в строке) и использовать параметр -f за которым следует имя файла.

В следующем примере мы запрашиваем домены, перечисленные в файле domains.txt .

domains.txt

lxer.com
linuxtoday.com
tuxmachines.org


dig -f domains.txt +short

108.166.170.171
70.42.23.121
204.68.122.43

Файл .digrc
Поведение команды dig можно контролировать, задав для каждого пользователя параметры в файле ${HOME}/.digrc .

Если файл .digrc присутствует в домашнем каталоге пользователя, указанные в нем параметры применяются перед аргументами командной строки.

Например, если вы хотите отобразить только раздел ответов, откройте текстовый редактор и создайте следующий файл ~/.digrc :

~/.digrc

+nocmd +noall +answer

Дополнительная проверка конфигурации bind

Запустите следующую команду для проверки синтаксиса файлов named.conf*:

sudo named-checkconf

Copy

Если в ваших именованных файлах конфигурации нет ошибок в синтаксисе, вы должны будете вернуться в командную строку без каких-либо сообщений об ошибках. При обнаружении проблем с файлами конфигурации вы должны будете просмотреть сообщение об ошибке и раздел «Настройка основного DNS сервера», а затем снова воспользоваться командой named-checkconf.

Команда named-checkzoneможет использоваться для проверки корректности ваших файлов зоны. Первый аргумент команды указывает имя зоны, а второй аргумент определяет соответствующий файл зоны, оба из которых определены в named.conf.local.

Например, чтобы проверить конфигурацию зоны для прямого просмотра “nyc3.example.com”, запустите следующую команду (измените на ваши имена зоны для прямого просмотра и файла):

sudo named-checkzone nyc3.example.com db.nyc3.example.com
Copy

А чтобы проверить конфигурацию зоны для обратного просмотра “128.10.in-addr.arpa”, запустите следующую команду (замените на данные, соответствующие вашей зоне для обратного просмотра и файлу):

sudo named-checkzone 128.10.in-addr.arpa /etc/bind/zones/db.10.128
Copy

Когда все файлы конфигурации и зоны не будут иметь ошибок, вы должны будете перезапустить службу BIND.

Перезапуск BIND

Перезапустите BIND:

sudo systemctl restart bind9

Copy

Если у вас есть настроенный брандмауэр UFW, откройте доступ к BIND с помощью следующей команды:

sudo ufw allow Bind9

Демоэкзамен
Виртуальные машины и коммутация.

Необходимо выполнить создание и базовую конфигурацию виртуальных машин.

На основе предоставленных ВМ или шаблонов ВМ создайте отсутствующие виртуальные машины в соответствии со схемой.
Перед началом выполнения задания рекомендуется внимательно ознакомиться со всеми пунктами задания, а так же разработать топологию и все необходимые документ для выполнения задания, Таким образом на основе задания была разработана топология и внесена адресация.


Характеристики ВМ установите в соответствии с Таблицей 1;

Перед началом выполнения задания также рекомендуется ознакомиться с виртуальным стендом, его характеристиками и требованиями

Таблица 1. Характеристики ВМ

Name VM
ОС
RAM
CPU
IP
Additionally
RTR-L
Debian 11/CSR
2 GB
2/4
4.4.4.100/24





192.168.100.254/24

RTR-R
Debian 11/CSR
2 GB
2/4
5.5.5.100/24





172.16.100.254 /24

SRV
Debian 11/Win 2019
2 GB /4 GB
2/4
192.168.100.200/24
Доп диски 2 шт по 5 GB
WEB-L
Debian 11
2 GB
2
192.168.100.100/24

WEB-R
Debian 11
2 GB
2
172.16.100.100/24

ISP
Debian 11
2 GB
2
4.4.4.1/24





5.5.5.1/24





3.3.3.1/24

CLI
Win 10
4 GB
4
3.3.3.10/24

Всю необходимую информацию можно увидеть в веб клиенте или в толстом клиенте


Коммутацию (если таковая не выполнена) выполните в соответствии со схемой сети.

Также перед началом работы убедитесь в том что все сегменты сети созданы и подключены к виртуальным интерфейсам


2. Имена хостов в созданных ВМ должны быть установлены в соответствии со схемой.

На устройстве RTR-L Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Для изменении имени на устройстве используем команду

1
hostname RTR-L


На устройстве RTR-L Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Для изменении имени на устройстве используем команду

1
hostname RTR-R


На устройстве SRV в Server Manager перейти в Local Server далее на Computer name затем в Change


В поел Computer name вводим имя нашего устройства далее OK после чего система выведет сообщение что изменения вступят в силу после перезагрузки устройства


Перезагружаем устройство Restart Now


На устройстве WEB-L Для изменении имени на используем команду

1
hostnamectl set-hostname WEB-L


На устройстве WEB-R Для изменении имени на используем команду

1
hostnamectl set-hostname WEB-R


На устройстве ISP Для изменении имени на используем команду

1
hostnamectl set-hostname ISP


На устройстве CLI необходимо запустить проводник на This PC правой кнопкой мыши вызвать меню прейти в Properties далее Change setting


Далее в Change В поел Computer name вводим имя нашего устройства далее OK после чего система выведет сообщение что изменения вступят в силу после перезагрузки устройства


3. Адресация должна быть выполнена в соответствии с Таблицей 1;

Перед началом настройки адресации необходимо разработать таблицу

Name VM
IP
Mask
Gat
DNS
RTR-L
4.4.4.100
255.255.255.0
4.4.4.1


192.168.100.254
255.255.255.0

192.168.100.200
RTR-R
5.5.5.100
255.255.255.0
5.5.5.1


172.16.100.254
255.255.255.0

192.168.100.200
SRV
192.168.100.200
255.255.255.0
192.168.100.254
192.168.100.200





WEB-L
192.168.100.100
255.255.255.0
192.168.100.254
192.168.100.200
WEB-R
172.16.100.100
255.255.255.0
172.16.100.254
192.168.100.200
ISP
4.4.4.1
255.255.255.0



5.5.5.1
255.255.255.0



3.3.3.1
255.255.255.0


CLI
3.3.3.10
255.255.255.0
3.3.3.1
3.3.3.1
На устройстве RTR-L Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Что бы получить информацию об интерфейсах на устройстве необходимо выполнить команду

1
show ip interface brif


Для того что бы получить информацию об конкретном интерфейсе и узнать его МАС адрес необходимо выполнить команду

1
show ip interface gigabitethernet1


На устройстве RTR-L перейти в настройки нажав правой клавишей мыши Settings


Выбрав Network adapter 1 далее Advanced сравнить МАС адрес интерфейса gigabitethernet1 и далее посмотреть сегмент к которому подключен данный интерфейс


На устройстве RTR-L Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Для назначения адреса на интерфейс gigabitethernet1 выполним команду

1
2
3
interface gigabitethernet1
ip address 4.4.4.100 255.255.255.0
no shutdown


Чтобы проверить выполнение команды и статус интерфейса выполним команду

1
show ip int br


Для назначения адреса на интерфейс gigabitethernet2 выполним команду

1
2
3
interface gigabitethernet2
ip address 192.168.100.254 255.255.255.0
no shutdown


Чтобы проверить выполнение команды и статус интерфейса выполним команду

1
show ip int br


На устройстве RTR-R Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Для того что бы получить информацию об конкретном интерфейсе и узнать его МАС адрес необходимо выполнить команду

1
show ip interface gigabitethernet1


На устройстве RTR-R перейти в настройки нажав правой клавишей мыши Settings


Выбрав Network adapter 1 далее Advanced сравнить МАС адрес интерфейса gigabitethernet1 и далее посмотреть сегмент к которому подключен данный интерфейс


На устройстве RTR-R Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Для назначения адреса на интерфейс gigabitethernet1 выполним команду

1
2
3
interface gigabitethernet1
ip address 5.5.5.100 255.255.255.0
no shutdown


Чтобы проверить выполнение команды и статус интерфейса выполним команду

1
show ip int br


Для назначения адреса на интерфейс gigabitethernet1 выполним команду

1
2
3
interface gigabitethernet1
ip address 172.16.100.254 255.255.255.0
no shutdown


Чтобы проверить выполнение команды и статус интерфейса выполним команду

1
show ip int br


На устройстве SRV в Server Manager перейти в Local Server далее на Ethernet0 далее прейти в Properties


в оснастке Ethernet0 Properties перейти в TCP/IPv4 указать назначение адреса статически и заполнить поля


Также для обеспечения работы ICMP протокола на устройстве SRV необходимо включить правило в firewall для этого на в Server manager необходимо перейти на вкладку Local Server далее в Windows Defender firewall затем на Advanced settings

ICMP, который расшифровывается как Internet Control Message Protocol это протокол третьего уровня модели OSI, который используется для диагностики проблем со связностью в сети.


Во входящих правилах найти правило file and printer sharing (echo request — icmpv4-in) далее Enable Rule


На устройстве WEB-L в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


Установим пакет network-manager для настройки сети в графическом интерфейсе

NetworkManager — программа для управления сетевыми соединениями в Linux.

1
apt install -y network-manager


После установки пакета необходимо сконфигурировать сетевой интерфейс используя NetworkManager для этого используем команду

1
nmtui


В графическом интерфейсе для изменения конфигурации сетевого интерфейса переходим в Edit a connection


Выбираем сетевой интерфейс на которым необходимо изменить конфигурацию Edit


В разделе IPv4 Configuration меняем с Automatic на Manual для назначения статической конфигурации


Далее переходим в show что бы отобразить необходимые поля


Вводим необходимую конфигурацию затем OK


Для того что бы конфигурация переменилась на интерфейс необходимо выключить и включить сетевой адаптер переходим в Activate a connection


Выбираем необходимый адаптер выключаем его клавишей пробел на клавиатуре Deactivate


После необходимо включить адаптер Activate далее OK


Для выхода из графического интерфейса выбираем OK


Для проверки используем команду

1
ip a


а также команду

1
2
ping 192.168.100.254
ping 192.168.100.200


На устройстве WEB-R в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


Установим пакет network-manager для настройки сети в графическом интерфейсе

NetworkManager — программа для управления сетевыми соединениями в Linux

1
apt install -y network-manager


После установки пакета необходимо сконфигурировать сетевой интерфейс используя NetworkManager для этого используем команду

1
nmtui


В графическом интерфейсе для изменения конфигурации сетевого интерфейса переходим в Edit a connection


Выбираем сетевой интерфейс на которым необходимо изменить конфигурацию Edit


В разделе IPv4 Configuration меняем с Automatic на Manual для назначения статической конфигурации . Далее переходим в show что бы отобразить необходимые поля . Вводим необходимую конфигурацию затем OK


Для того что бы конфигурация переменилась на интерфейс необходимо выключить и включить сетевой адаптер переходим в Activate a connection


Выбираем необходимый адаптер выключаем его клавишей пробел на клавиатуре Deactivate


После необходимо включить адаптер Activate далее OK


Для выхода из графического интерфейса выбираем OK


Для проверки используем команду

1
ip a


А также команду

1
ping 172.16.100.254


На устройстве ISP в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


Установим пакет network-manager для настройки сети в графическом интерфейсе, а также bind9 и chronybind9 это пакет создающий DNS-сервер в локальной или глобальной сети. Bind9 может также работать и в режиме кеширующего DNS-сервера.

Chrony это программа создающая сервер времени на рабочей станции и синхронизирующая локальное время с глобальным в Интернете.

1
apt install -y network-manager bind9 chrony


После установки пакета необходимо сконфигурировать сетевой интерфейс используя NetworkManager для этого используем команду

1
nmtui


Выбираем сетевой интерфейс Wired connection 1 на которым необходимо изменить конфигурацию Edit


На устройстве ISP перейти в настройки нажав правой клавишей мыши Settings


Выбрав Network adapter 1 далее Advanced сравнить МАС адрес интерфейса Wired connection 1 и далее посмотреть сегмент к которому подключен данный интерфейс


В разделе IPv4 Configuration меняем с Automatic на Manual для назначения статической конфигурации .


Далее переходим в show что бы отобразить необходимые поля . Вводим необходимую конфигурацию затем OK


Выбираем сетевой интерфейс Wired connection 2 на которым необходимо изменить конфигурацию Edit


Выбрав Network adapter 2 далее Advanced сравнить МАС адрес интерфейса Wired connection 2 и далее посмотреть сегмент к которому подключен данный интерфейс


В разделе IPv4 Configuration меняем с Automatic на Manual для назначения статической конфигурации .


Далее переходим в show что бы отобразить необходимые поля . Вводим необходимую конфигурацию затем OK


Выбираем сетевой интерфейс Wired connection 3 на которым необходимо изменить конфигурацию Edit


Выбрав Network adapter 3 далее Advanced сравнить МАС адрес интерфейса Wired connection 3 и далее посмотреть сегмент к которому подключен данный интерфейс


В разделе IPv4 Configuration меняем с Automatic на Manual для назначения статической конфигурации .


Далее переходим в show что бы отобразить необходимые поля . Вводим необходимую конфигурацию затем OK


Для того что бы конфигурация переменилась на интерфейс необходимо выключить и включить сетевой адаптер переходим в Activate a connection


Выбираем все необходимый адаптер выключаем его клавишей пробел на клавиатуре Deactivate


После необходимо включить адаптеры Activate далее OK


Для выхода из графического интерфейса выбираем OK


Для проверки используем команду

1
ip a


А также команду

1
2
ping 4.4.4.100
ping 5.5.5.100


На устройстве CLI Открываем control panel далее network and internet


Далее в Network and sharing Center выбираем Ethernet0 затем Properties


в оснастке Ethernet0 Properties перейти в TCP/IPv4 указать назначение адреса статически и заполнить поля


4. Обеспечьте ВМ дополнительными дисками, если таковое необходимо в соответствии с Таблицей 1;

На устройстве SRV перейти в настройки нажав правой клавишей мыши Settings


Убедимся что создана Hard disk 2 и Hard disk 3


Сетевая связность.

В рамках данного модуля требуется обеспечить сетевую связность между регионами работы приложения, а также обеспечить выход ВМ в имитируемую сеть “Интернет”

Сети, подключенные к ISP, считаются внешними:
На устройстве ISP пересылка трафика между интерфейсами В операционной системе Linux включается командой net.ipv4.ip_forward=1 для этого необходимо отредактировать конфигурационный файл который расположен в директории /etc/sysctl.conf

nano — консольный текстовый редактор для UNIX и Unix-подобных операционных систем, основанный на библиотеке curses и распространяемый под лицензией GNU GPL.

1
nano /etc/sysctl.conf


В файле с конфигурацией находим #net.ipv4.ip_forward=1 и убираем #

1
net.ipv4.ip_forward=1


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Для того что бы конфигурация применилась необходимо выполнить команду

1
sysctl -p


sysctl — утилита, предназначенная для управления параметрами ядра на лету

На устройстве RTR-L Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Необходимо задать Статический маршрут при помощи команды глобальной конфигурации ip route.

1
ip route 0.0.0.0 0.0.0.0 4.4.4.1


На устройстве RTR-R Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Необходимо задать Статический маршрут при помощи команды глобальной конфигурации ip route.

1
ip route 0.0.0.0 0.0.0.0 5.5.5.1


Для проверки статического маршрута на устройстве RTR-R используем команду

1
show ip route


Для проверки статического маршрута на устройстве RTR-L

1
ping 5.5.5.1


Для проверки статического маршрута на устройстве RTR-R

1
ping 4.4.4.1


2. Платформы контроля трафика, установленные на границах регионов, должны выполнять трансляцию трафика, идущего из соответствующих внутренних сетей во внешние сети стенда и в сеть Интернет.

На устройстве RTR-L Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Статический NAT представляет собой сопоставление внутреннего и внешнего адреса один к одному. Он позволяет внешним устройствам инициировать подключения к внутренним с использованием статически назначенного общего адреса.

PAT (также называемый NAT overload) сохраняет адреса во внутреннем глобальном пуле адресов, позволяя маршрутизатору использовать один внутренний глобальный адрес для многих внутренних локальных адресов

Рассмотрим настойку PAT для пула адресов по шагам:

Определяем, какие интерфейсы находятся внутри, по отношению к NAT, а какие снаружи. Используем команду  ip nat outside

1
2
interface gigabitethernet 1
ip nat outside


Определяем, какие интерфейсы находятся внутри, по отношению к NAT, а какие снаружи. Используем команд ip nat inside 

1
2
interface gigabitethernet 2
ip nat inside


Создать лист access-list разрешающий адреса, которые нужно транслировать – access-list [номер_ACL] permit source [wildcard_маска].

1
access-list 1 permit 192.168.100.0 0.0.0.255


Настроить преобразование адреса источника в адрес интерфейса, через команду ip nat inside source list [номер_ACL] interface [тип номер] overload

1
ip nat inside source list 1 interface Gi1 overload


На устройстве RTR-R Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Определяем, какие интерфейсы находятся внутри, по отношению к NAT, а какие снаружи. Используем команду  ip nat outside

1
2
interface gigabitethernet 1
ip nat outside


Определяем, какие интерфейсы находятся внутри, по отношению к NAT, а какие снаружи. Используем команд ip nat inside 

1
2
interface gigabitethernet 2
ip nat inside


Создать лист access-list разрешающий адреса, которые нужно транслировать – access-list [номер_ACL] permit source [wildcard_маска].

1
access-list 1 permit 172.16.100.0 0.0.0.255


Настроить преобразование адреса источника в адрес интерфейса, через команду ip nat inside source list [номер_ACL] interface [тип номер] overload

1
ip nat inside source list 1 interface Gi1 overload


Для проверки на устройстве RTR-L используем команду

1
show ip nat statistics


Для проверки на устройстве RTR-L используем команду

1
show ip nat statistics


3. Между платформами должен быть установлен защищенный туннель, позволяющий осуществлять связь между регионами с применением внутренних адресов.

На устройстве RTR-L Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Generic Routing Encapsulation (GRE) — это протокол туннелирования, разработанный компанией Cisco, который позволяет инкапсулировать широкий спектр протоколов сетевого уровня в point-to-point каналах.

Туннель GRE использует интерфейс «туннель» — логический интерфейс, настроенный на маршрутизаторе с IP-адресом, где пакеты инкапсулируются и декапсулируются при входе или выходе из туннеля GRE.

В нашем примере оба туннельных интерфейса являются частью сети 172.16.1.0/24.

1
2
3
4
5
interface Tunne 1
ip address 172.16.1.1 255.255.255.0
tunnel mode gre ip
tunnel source 4.4.4.100
tunnel destination 5.5.5.100


На устройстве RTR-R Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Generic Routing Encapsulation (GRE) — это протокол туннелирования, разработанный компанией Cisco, который позволяет инкапсулировать

1
2
3
4
5
interface Tunne 1
ip address 172.16.1.2 255.255.255.0
tunnel mode gre ip
tunnel source 5.5.5.100
tunnel destination 4.4.4.100


На устройстве RTR-L для проверки туннеля используем команду

1
ping 172.16.1.1


На устройстве RTR-R для проверки туннеля используем команду

1
ping 172.16.1.1


протокол EIGRP (Enhanced Interior Gateway Routing Protocol)— протокол маршрутизации.

включения протокола глобальной командой router eigrp ASN_NUMBER

выбора сетей, которые протокол будет «вещать», для чего используется команда(ы) network

1
2
3
router eigrp 6500
network 192.168.100.0 0.0.0.255
network 172.16.1.0 0.0.0.255


включения протокола глобальной командой router eigrp ASN_NUMBER

выбора сетей, которые протокол будет «вещать», для чего используется команда(ы) network

1
2
3
router eigrp 6500
network 172.16.100.0 0.0.0.255
network 172.16.1.0 0.0.0.255


На устройстве RTR-L для проверки используем команду

1
show ip protocols


На устройстве RTR-R для проверки используем команду

1
show ip protocols


На устройстве RTR-L для проверки используем команду

1
ping 172.16.100.100


На устройстве RTR-R для проверки используем команду

1
ping 192.168.100.100


Трафик, проходящий по данному туннелю, должен быть защищен

На устройстве RTR-L Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
IPSec для добавления уровня шифрования и защиты туннеля GRE. Это обеспечивает нам необходимое шифрование военного уровня и спокойствие. Наш пример ниже охватывает режим туннеля GRE IPSec

Шифрование IPSec включает в себя два этапа для каждого маршрутизатора. Эти шаги

Первым шагом является настройка политики ISAKMP Phase 1:

1
2
3
4
5
crypto isakmp policy 1
encr aes
authentication pre-share
hash sha256
group 14
aes — метод шифрования, который будет использоваться на этапе 1 Phase 1

Authentication pre-share — использование предварительного общего ключа в качестве метода проверки подлинности

sha256 — алгоритм хеширования

Group 14 — группа Диффи-Хеллмана, которая будет использоваться


Далее мы собираемся определить Pre Shared Key (PSK) для аутентификации с партнером

1
2
crypto isakmp key TheSecretMustBeAtLeast13bytes address 5.5.5.100
crypto isakmp nat keepalive 5


Теперь нам нужно создать набор преобразований, используемый для защиты наших данных. 

1
2
crypto ipsec transform-set TSET  esp-aes 256 esp-sha256-hmac
mode tunnel
Установите IPSec в транспортный режим.


создаем профиль IPSec для соединения ранее определенной конфигурации ISAKMP и IPSec. Мы назвали наш профиль IPSec protect-gre

1
2
crypto ipsec profile VTI
set transform-set TSET


применить шифрование IPSec к интерфейсу туннеля

1
2
3
interface Tunnel1
tunnel mode ipsec ipv4
tunnel protection ipsec profile VTI


На устройстве RTR-R Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Первым шагом является настройка политики ISAKMP Phase 1:

1
2
3
4
5
crypto isakmp policy 1
encr aes
authentication pre-share
hash sha256
group 14


Далее мы собираемся определить Pre Shared Key (PSK) для аутентификации с партнером

1
2
crypto isakmp key TheSecretMustBeAtLeast13bytes address 4.4.4.100
crypto isakmp nat keepalive 5


Теперь нам нужно создать набор преобразований, используемый для защиты наших данных. 

1
2
crypto ipsec transform-set TSET  esp-aes 256 esp-sha256-hmac
mode tunnel


создаем профиль IPSec для соединения ранее определенной конфигурации ISAKMP и IPSec. Мы назвали наш профиль IPSec protect-gre

1
2
crypto ipsec profile VTI
set transform-set TSET


применить шифрование IPSec к интерфейсу туннеля

1
2
3
interface Tunnel1
tunnel mode ipsec ipv4
tunnel protection ipsec profile VTI


На устройстве RTR-L командой ping инициируем сессию а далее проверяем статус шифрованного туннеля

1
2
ping 172.16.100.254
show crypto session


4. Платформа управления трафиком RTR-L выполняет контроль входящего трафика согласно следующим правилам

На устройстве RTR-L Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
ACL (Access Control List) — это набор текстовых выражений, которые что-то разрешают, либо что-то запрещают. Обычно ACL разрешает или запрещает IP-пакеты, но помимо всего прочего он может заглядывать внутрь IP-пакета, просматривать тип пакета, TCP и UDP порты. Также ACL существует для различных сетевых протоколов (IP, IPX, AppleTalk и так далее). 

создаем ACL с именем Lnew. В качестве аргумента команды ip access-list необходимо указать не только имя, но и тип ACL (standard – стандартный и extended – расширенный), так как маршрутизатор не может сам определить тип.

1
ip access-list extended Lnew


Разрешаются подключения к портам HTTP и HTTPS для всех клиентов

1
2
3
4
permit udp host 4.4.4.100 eq 53 any
permit tcp host 4.4.4.100 eq 53 any
permit tcp any host 4.4.4.100 eq 80
permit tcp any host 4.4.4.100 eq 443


Порты необходимо для работы настраиваемых служб

1
2
3
4
5
permit udp host 5.5.5.100 host 4.4.4.100 eq 500
permit esp any any
permit tcp any any established
permit udp host 5.5.5.1 eq 123 any
permit udp host 4.4.4.1 eq 123 any


Разрешается работа протоколов ICMP

1
permit icmp any any


Разрешается работа протокола SSH

1
permit tcp any host 4.4.4.100 eq 2222


после создания ACL его необходимо будет применить в соответствии с вашими целями и задачами. То что, касается фильтрации, то ACL применяется на интерфейсе

1
2
interface gigabitethernet 1
ip access-group Lnew in


Для проверки используем команду

1
show access-list


5. Платформа управления трафиком RTR-R выполняет контроль входящего трафика согласно следующим правилам

На устройстве RTR-R Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
создаем ACL с именем Rnew. В качестве аргумента команды ip access-list необходимо указать не только имя, но и тип ACL (standard – стандартный и extended – расширенный), так как маршрутизатор не может сам определить тип.

1
ip access-list extended Rnew


Разрешаются подключения к портам HTTP и HTTPS для всех клиентов

1
2
permit tcp any host 5.5.5.100 eq 80
permit tcp any host 5.5.5.100 eq 443


Порты необходимо для работы настраиваемых служб

1
2
3
permit udp host 4.4.4.100 host 5.5.5.100 eq 500
permit esp any any
permit tcp any any established


Разрешается работа протоколов ICMP

1
permit icmp any any


Разрешается работа протокола SSH

1
permit tcp any host 5.5.5.100 eq 2244


после создания ACL его необходимо будет применить в соответствии с вашими целями и задачами. То что, касается фильтрации, то ACL применяется на интерфейсе

1
2
interface gigabitethernet 1
ip access-group Rnew in


Для проверки используем команду

1
show access-list


6. Обеспечьте настройку служб SSH региона Left и Right

На устройстве RTR-L Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Подключения со стороны внешних сетей по протоколу к платформе управления трафиком RTR-L на порт 2222 должны быть перенаправлены на ВМ Web-L

Создаем и статическую PAT-трансляцию для порта

1
ip nat inside source static tcp 192.168.100.100 22 4.4.4.100 2222


На устройстве RTR-R Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Подключения со стороны внешних сетей по протоколу к платформе управления трафиком RTR-R на порт 2244 должны быть перенаправлены на ВМ Web-R;

Создаем и статическую PAT-трансляцию для порта

1
ip nat inside source static tcp 172.16.100.100 22 5.5.5.100 2244


На устройстве WEB-L в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


SSH (Secure SHell — защищенная оболочка) — сетевой протокол прикладного уровня, предназначеный для безопасного удаленного доступа к UNIX-системам. Данный протокол эффективен тем, что шифрует всю передаваемую информацию по сети. По умолчанию, используется 22-й порт

Установим пакет openssh-server ssh командой

1
apt install -y openssh-server ssh


После установки службы ее необходимо запустить и добавить в автоматический запуск командой

1
2
systenctl start sshd
systenctl enavle ssh


Для проверки работоспособности службы необходимо использовать команду

1
systenctl status sshd


На устройстве WEB-R в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


Установим пакет openssh-server ssh командой

1
apt install -y openssh-server ssh


Для проверки работоспособности службы необходимо использовать команду


После установки службы необходимо отредактировать конфигурационный файл расположенный в директории /etc/ssh/sshd_config на устройстве WEB-L

1
nano /etc/ssh/sshd_config


В блоке # Authent находим PermitRooLogin и меняем ее для того что бы дать пользователю root возможность подключаться по ssh

1
PermitRooLogin yes


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Чтобы конфигурация применилась необходимо перезагрузить службу командой

1
systemctl restart sshd


Для проверки на устройстве CLI необходимо запустить powershell и использовать команду

1
ssh root@4.4.4.100 -p 2222


После установки службы необходимо отредактировать конфигурационный файл расположенный в директории /etc/ssh/sshd_config на устройстве WEB-R

1
nano /etc/ssh/sshd_config


В блоке # Authent находим PermitRooLogin и меняем ее для того что бы дать пользователю root возможность подключаться по ssh

1
PermitRooLogin yes


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Чтобы конфигурация применилась необходимо перезагрузить службу командой

1
systemctl restart sshd


Для проверки на устройстве CLI необходимо запустить powershell и использовать команду

1
ssh root@5.5.5.100 -p 2244


Инфраструктурные службы

В рамках данного модуля необходимо настроить основные инфраструктурные службы и настроить представленные ВМ на применение этих служб для всех основных функций

Выполните настройку первого уровня DNS-системы стенда
На устройстве ISP в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


Установим пакет bind9 командой

bind9 это пакет создающий DNS-сервер в локальной или глобальной сети. Bind9 может также работать и в режиме кеширующего DNS-сервера.

1
apt install -y bind9


После установки службы bind9 необходимо создать директорию /opt/dns

1
mkdir /opt/dns


Далее необходимо скопировать конфигурационный файл db.local в созданию директорию командой

1
cp /etc/bind/db.local /opt/dns/demo.db


При помощи команды chown необходимо назначить права доступ на директорию /opt/dns

1
chown -R bind:bind /opt/dns


Также для работа способности службы из директории /opt/dns необходимо отредактировать конфигурационный файл службы apparmor

AppArmor — это реализация Модуля безопасности линукс по управлению доступом на основе имен. AppArmor ограничивает отдельные программы набором перечисленных файлов и возможностями в соответствии

1
nano /etc/apparmor.d/usr.sbin.named


AppArmor устанавливается и загружается по умолчанию. Он использует профили приложений для определения какие файлы и права доступа требуются приложению. Некоторые пакеты устанавливают свои собственные профили, а дополнительные профили можно найти в пакете apparmor-profiles.

1
/opt/dns/** rw,


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Что бы изменения вошли в силу необходимо перезагрузить службу командой

1
systemctl restart apparmor.service


После установки сервера необходимо указать некоторые параметры для работы сервера. Это делается путем редактирования определенных директив в файле /etc/bind/named.conf.options:

1
nano /etc/bind/named.conf.options


dnssec-validation — проверка корректность ответов; allow-query — а этот параметр указывает кому разрешается подавать запросы к нашему серверу.

1
2
dnssec-validation no;
allow-query { any; };


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


bind9 поставляется с файлом, содержащим зоны прямой, обратной и широковещательной передачи по умолчанию в директории /etc/bind/named.conf.default-zones

1
nano /etc/bind/named.conf.default-zones


zone «demo.wsr» — Начало части «Zone — Master».type master — Тип зоны. master = primaryallow-transfer — устанавливает возможность передачи зон для slave-серверов. В нашем случае трансфер запрещен.file «/opt/dns/demo.db»; — Имя файла, в котором хранится таблица DNS

Необходимо заменить имя зоны по умолчанию localhost

1
2
3
4
5
zone "demo.wsr" {
   type master;
   allow-transfer { any; };
   file "/opt/dns/demo.db";
};


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Далее необходимо отредактировать скопированный файл зона домена demo.wsr полный файл зоны для домена

1
nano /opt/dns/demo.db


Заменяем @ IN SOA на запись вида

1
@ IN SOA demo.wsr. root.demo.wsr.(


Таблица 2. DNS-записи зон

Zone
Type
Key
Meaning
demo.wsr
A
isp
3.3.3.1

A
www

4.4.4.100

A
www
5.5.5.100

CNAME
internet
isp

NS
int
rtr-l.demo.wsr

A
rtr-l
4.4.4.100
В соответствии с таблицей создадим записи для зоны demo.wsr

1
2
3
4
5
6
7
@ IN NS isp.demo.wsr.
isp IN A 3.3.3.1
www IN A 4.4.4.100
www IN A 5.5.5.100
internet CNAME isp.demo.wsr.
int IN NS rtr-l.demo.wsr.
rtr-l IN  A 4.4.4.100


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Для того что бы изменения вступили в силлу необходимо перезагрузить сервис

1
systemctl restart bind9


Для проверки работа способности службы можно использовать команду

1
systemctl status bind9


Далее на CLI необходимо запустить powershell и указать команду для проверки DNS службы , также нужно проверить что на интерфейсе указан DNS сервер

1
nslookup www.demo.wsr


Маршрутизатор региона должен транслировать соответствующие порты DNS-службы в порты сервера SRV

На устройстве RTR-L Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Для настройки статического NAT используется команда

1
ip nat inside source static tcp 192.168.100.200 53 4.4.4.100 53


А также команду

1
ip nat inside source static udp 192.168.100.200 53 4.4.4.100 53


2. Выполните настройку второго уровня DNS-системы стенда;

Перед началом установки необходимо убедиться что на интерфейсе устройства SRV добавлены DNS сервера


На устройстве SRV в оснастке server manager переходим на вкладку Manage далее Add Roles and Features


Появляется помощник по установки после ознакомления нажимаем next


Выбираем тип установки Role-based or feature-based installation для того что бы установить роль или дополнительные оснастки


Установка и настройка ролей осуществляется на в кладке Manage также есть возможность управлениями несколькими удаленными серверами


Установка роли осуществляется отметкой в check box при выборе роли есть краткое описание ее назначения


При нажатии на Add Features устанавливаются все необходимые минимальные оснастки для работы этой роли


Также можно выбрать несколько ролей для установки после этого переходим next


Также можно выбрать несколько дополнительных оснасток для установки после этого переходим next


После ознакомления с описанием роли продолжаем этап установки next


В ходе установки и настройки может понадобиться перезагрузка при установке отметки сервер это сделает в автоматическом режиме


По окончанию установки можно закрыть оснастку для установки ролей


На устройстве SRV в оснастке server manager переходим на вкладку Tools далее DNS


В оснастке DNS Manager развернем иерархию и на вкладке Forward Lookup Zones необходимо нажать правой кнопкой мыши, а далее New Zone


Откроется оснастка по настройки зоны DNS нажимаем next


Выбираем тип зоны Primary Zone и переходим next


Указываем имя зоны int.demo.wsr


Автоматически создастся имя файла в котором будет располагаться записи


Оставляем демоническое обновление зоны по умолчанию переходим next


По окончанию оснастка покажет конфигурацию которая была выбрана и переходим finish


Далее в созданной зоне необходимо создать записи из приведённой ниже таблицы

Zone
Type
Key
Meaning
int.demo.wsr
A
web-l
192.168.100.100

A
web-r
172.16.100.100

A
srv

192.168.100.200

A
rtr-l
192.168.100.254

A
rtr-r
172.16.100.254

CNAME
webapp1
web-l

CNAME
webapp2
web-r

CNAME
ntp
srv

CNAME
dns
srv
Для создания записи в оснастке DNS Manager необходимо развернуть необходимо развернуть иерархию выбрать созданную зону int.demo.wsr и в свободном месте нажать правой кнопкой мыши и выбрать New Host (A or AAAA)


В поле Name указываем web-l в поле ip adddress указываем 192.168.100.100 и переходим add host


Далее по аналогии с записью web-l создаем записи типа A


Для создания записи в оснастке DNS Manager необходимо развернуть необходимо развернуть иерархию выбрать созданную зону int.demo.wsr и в свободном месте нажать правой кнопкой мыши и выбрать New Alias (CNAME)


В поле alias name указываем webapp1 в поле fully qualifed domain name переходим в browse разворачиваем иерархию и выбираем запись web-l


После проверяем выбранные параметры запись и переходим ok


Далее по аналогии с записью webapp1 создаем записи типа CNAME


Далее на CLI необходимо запустить powershell и указать команду

1
nslookup srv.int.demo.wsr


Также на устройстве ISP указать команду

1
host ntp.int.demo.wsr


3. Выполните настройку первого уровня системы синхронизации времени

Протокол сетевого времени (NTP) — это протокол, который помогает синхронизировать время устройств (компьютеров, серверов, коммутаторов, маршрутизаторов) в сети.NTP для синхронизации использует протокол UDP и 123 порт

На устройстве ISP в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


Далее установим bind9, который реализует сервер DNS если он не был установлен ранее

1
apt install -y chrony


Необходимо отредактировать конфигурационный файл который расположен в директории /etc/chrony/chrony.conf

1
nano /etc/chrony/chrony.conf


В блоке # Use Debian vendor zone необходимо добавить следующую конфигурацию

1
2
3
local stratum 4
allow 4.4.4.0/24
allow 3.3.3.0/24
local stratum — уровень работы иерархии синхронизации времениallow — разрешенные сети


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Для того что бы конфигурация перезагрузить необходимо перезагрузить сервис командой

1
systemctl restart chronyd


Также добавим сервис в автоматическую загрузку

1
systemctl enable chrony


Что бы проверить статус службы используем команду

1
systemctl status chronyd


4. Выполните конфигурацию службы второго уровня времени на SRV

NTP для синхронизации использует протокол UDP и 123 порт по этому на SRv необходимо открыть данный порт в Fierwall

На устройстве SRV в оснастке server manager переходим на вкладку local server далее Private:On


Перейдем в расширенные настройки правил Advanced settings


Перейдем на входящие правила Inbound Rules далее New Rule для создания правила


Выбираем тип Port далее next


Выбираем протокол UDP и указываем имя порта далее next


Указываем разжените на подключение далее next


Так же по умолчанию будут выбраны все профили для распространения данного правила


Задаем понятное имя для правила и далее finish


Для синхронизации время с хостом ISP в server manager перейдем на local server далее на (UTC-08:00)


В оснастке Date and Time перейдем на вкладку Internet Time затем выберем Change serrings для изменения конфигурации указываем сервер в поле server с кум будет установлена синхронизация далее update now далее ok


Для проверки применения конфигурации в powershell можно использовать команду

1
w32tm /query /source


Все внутренние хосты(в том числе и платформы управления трафиком) должны синхронизировать свое время с SRV;

На устройстве RTR-L Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Команда ip domain name используется для определения имени домена, которое будет использоваться для дополнения неполных имен хостовКоманда ip name-server применяется для настройки ip-адресов DNS-серверов

1
2
ip domain name int.demo.wsr
ip name-server 192.168.100.200


Команда ntp server используется для указания сервера для синхронизации времени

1
ntp server ntp.int.demo.wsr


Для проверки синхронизации можно использовать команду

1
show ntp associations


На устройстве RTR-R Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Команда ip domain name используется для определения имени домена, которое будет использоваться для дополнения неполных имен хостовКоманда ip name-server применяется для настройки ip-адресов DNS-серверов


Команда ntp server используется для указания сервера для синхронизации времени

1
ntp server ntp.int.demo.wsr


Для проверки синхронизации можно использовать команду

1
show ntp associations


На устройстве WEB-L в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


Установим пакет для службы chrony

1
apt install -y chrony


Далее необходимо отредактировать конфигурационный файл который находиться /etc/chrony/chrony.conf

1
nano /etc/chrony/chrony.conf


Указываем источник синхронизации времени и разрешенные сети

1
2
pool ntp.int.demo.wsr iburst
allow 192.168.100.0/24


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Для применения конфигурации необходимо перезагрузить сервис

1
systemctl restart chrony


Для добавления в автоматическую загрузку используем команду

1
systemctl enable chrony


Также для проверки синхронизации времени можно использовать команду

1
chronyc sources


На устройстве WEB-R в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


Установим пакет для службы chrony

1
apt install -y chrony


Далее необходимо отредактировать конфигурационный файл который находиться /etc/chrony/chrony.conf

1
nano /etc/chrony/chrony.conf


Указываем источник синхронизации времени и разрешенные сети

1
2
pool ntp.int.demo.wsr iburst
allow 192.168.100.0/24


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Для применения конфигурации необходимо перезагрузить сервис

1
systemctl restart chrony


Для добавления в автоматическую загрузку используем команду

1
systemctl enable chrony


Также для проверки синхронизации времени можно использовать команду

1
chronyc sources


На устройстве CLI Открываем control panel далее Clock and Region


Переходим в Date and Time далее в Internet Time для изменения конфигурации в Change settings указываем адрес для синхронизации в поле server далее update now и ok


Для проверки конфигурации необходима открыть powershell от имени Администратора и ввести команду

1
w32tm /query /source


5. Реализуйте файловый SMB-сервер на базе SRV

RAID — это распространенный метод защиты данных приложений на жестких дисках и твердотельных накопителях, причем в различных типах RAID уровень защиты уравновешивается ценой. 

RAID 1 (“Mirroring”, «зеркало»). Он имеет защиту от выхода из строя половины имеющихся аппаратных средств (в общем случае – одного из двух жестких дисков), обеспечивает приемлемую скорость записи и выигрыш по скорости чтения за счет распараллеливания запросов. Недостаток заключается в том, что приходится выплачивать стоимость двух жестких дисков, получая полезный объем одного жесткого диска.

На устройстве SRV в оснастке server manager переходим на вкладку File and Storage Services


Переходим в Starage pools далее в tasks и New Storade Pool


Открывается оснастка для создания пула переходим next


В поле name задаем имя для дискового пула переходим next


Выбираем все доступные диски которые войдут в состав дискового пула переходим next


Проверяем выбранную конфигурацию переходим create


После завершения процесса создания необходимо в дисковом пуле создать виртуальный диск и выбрать сценарий хранения RAID 1, ставим отметку в разделе Create a virtual disk when this wizard clonses и переходим в close


В относки выбираем пул дисков который был создан переходим в ok


Открывается оснастка для создания виртуального диска переходим в next


В поле name задаем имя виртуального диска переходим в next


оставляем конфигурацию по умолчанию Переходим в next


задаем сценарий хранения Mirror он будят являться RAID 1 переходим next


Тип выделения памяти для виртуального диска выбираем Fixed переходим next


Выбираем Maximum size что бы выделить все место переходим next


Проверяем в оснастке выбранную конфигурацию переходим create


После завершения создания виртуального диска необходимо создать раздел для хранения и отформатировать его выбираем create a volume when this wizard closes переходим в close


Открывается оснастка для создания радела на виртуальном диске переходим next


Выбираем диск для которого будет создан раздел переходим next


Задаем максимальный размер для раздела переходим next


Далее задаем необходимую букву на раздел переходим next


Задаем тип форматирования раздела NTFS в поле File system и имя в поле Volume label RAID1


Проверяем выбранную конфигурацию переходим в create


Для проверки на устройстве SRV откроем This PC где будет располагаться раздел RAID1 с буквой D, Оснастке sarver manager в File and storage далее в Staroge pools выбрать пул POOLRAID1 в разделе virtual disks в поле layot можно увидеть сценарий хранения mirror


Протокол SMB — это сетевой протокол для общего доступа к файлам, который позволяет приложениям компьютера читать и записывать файлы, а также запрашивать службы серверных программ в компьютерной сети. Протокол SMB может использоваться поверх протокола TCP/IP или других сетевых протоколов. 

На устройстве SRV в оснастке server manager переходим на вкладку local server в manage далее в add roles and features


Появляется помощник по установки после ознакомления нажимаем next


Выбираем тип установки Role-based or feature-based installation для того что бы установить роль или дополнительные оснастки


Установка и настройка ролей осуществляется на в кладке Manage также есть возможность управлениями несколькими удаленными серверами


Для реализации работы протокола SMB нам понадобиться роль File server разворачиваем File and storage services далее File and iSCSI Services и выбираем File Server переходим next


Также можно выбрать несколько ролей для установки после этого переходим next


В ходе установки и настройки может понадобиться перезагрузка при установке отметки сервер это сделает в автоматическом режиме


По окончанию установки можно закрыть оснастку для установки ролей


После установки роли необходимо создать общее пространства используя протокол SMB для этого в server manager переходим в раздел File and Storade Services


Переходим в Shares далее в TASKS затем New Share


Выбираем SMB Share — Quick для быстрой конфигурации общего ресурса переходим next


Необходимо выбрать физическое расположение общего ресурса переходим в Browse выбираем RAID1 создаем новую директорию задаем ей имя storage далее переходим select folder


После того как физическое расположение директории общего ресурса выбрано переходим next


Задаем логическое имя для обнаружения общего ресурса в сети в поле share name имя storage


Выбираем дополнительную опцию для того что бы только те пользователи у которых есть права могли видеть общей ресурс в сети переходим next


Необходимо назначить права доступа на общий сетевой ресурс выбираем Customize permissions далее в Share на группу пользователей Everyone далее edit


Для данной группы назначаем все права доступа Full Control переходим ok


После назначения прав переходим Apply проверим выбранную конфигурацию далее Create


По окончанию процесса создания переходим Close


На устройстве WEB-L в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


Установим пакет cifs-utils Протокол SMB/CIFS — средство обмена файлами между компьютерами, Этот пакет содержит утилиты для монтирования сетевых файловых систем CIFS

1
apt install -y cifs-utils


Для подключения общего ресурса изменим конфигурационный файл .smbclient командой

1
nano /root/.smbclient


Добавим пользователя для подключения к общему ресурсу

1
2
username=Administrator
password=Pa$$w0rd


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Далее необходимо сконфигурировать файл для подключения общего ресурса расположенного в директоре /etc/fstab

1
nano /etc/fstab


Добавим точку монтирования командой

1
//srv.int.demo.wsr/storage /opt/share cifs user,rw,_netdev,credentials=/root/.smbclient 0 0


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Создадим директорию для монтирование общего сетевого ресурса в системе командой

1
mkdir /opt/share


После настройки конфигурационных файлов и создание директории для монтирование необходимо ввести команду

1
mount -a


На устройстве WEB-R в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


Установим пакет cifs-utils Протокол SMB/CIFS — средство обмена файлами между компьютерами, Этот пакет содержит утилиты для монтирования сетевых файловых систем CIFS

1
apt install -y cifs-utils


Для подключения общего ресурса изменим конфигурационный файл .smbclient командой

1
nano /root/.smbclient


Добавим пользователя для подключения к общему ресурсу

1
2
username=Administrator
password=Pa$$w0rd


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Далее необходимо сконфигурировать файл для подключения общего ресурса расположенного в директоре /etc/fstab

1
nano /etc/fstab


Добавим точку монтирования командой

1
//srv.int.demo.wsr/storage /opt/share cifs user,rw,_netdev,credentials=/root/.smbclient 0 0


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Создадим директорию для монтирование общего сетевого ресурса в системе командой

1
mkdir /opt/share


После настройки конфигурационных файлов и создание директории для монтирование необходимо ввести команду

1
mount -a


6. Сервера WEB-L и WEB-R должны использовать службу, настроенную на SRV, для обмена файлами между собой:

Для проверки на устройстве SRV в директории storage создадим пустой файл с именем testWEB-L формата txt


На устройстве WEB-L используя команду отредактируем файл

1
nano /opt/share/testWEB-L.txt


Добавим запись и сохраним


Далее на устройстве SRV откроем данный файл и убедимся что изменения вошли в силу


Для проверки на устройстве SRV в директории storage создадим пустой файл с именем testWEB-R формата txt


На устройстве WEB-L используя команду отредактируем файл

1
nano /opt/share/testWEB-R.txt


Добавим запись и сохраним


Далее на устройстве SRV откроем данный файл и убедимся что изменения вошли в силу


7. Выполните настройку центра сертификации на базе SRV:

PKI является технологией безопасности, которая основывается на стандарте X.509 и использует цифровые сертификаты. Целью PKI является повышение безопасности ИТ инфраструктуры предприятия за счёт предоставления механизмов по защите данных от несанкционированного доступа и незаконного изменения данных (подделка данных).

На устройстве SRV в Server Manager перейти в add roles and features


Появляется помощник по установки после ознакомления нажимаем next


Выбираем тип установки Role-based or feature-based installation для того что бы установить роль или дополнительные оснастки


Установка и настройка ролей осуществляется на в кладке Manage также есть возможность управлениями несколькими удаленными серверами


Установка роли осуществляется отметкой в check box при выборе роли есть краткое описание ее назначения. При нажатии на Add Features устанавливаются все необходимые минимальные оснастки для работы этой роли


Также можно выбрать несколько ролей для установки после этого переходим next


Также можно выбрать несколько дополнительных оснасток для установки после этого переходим next


После ознакомления с описанием роли продолжаем этап установки next


Также необходимо выбрать дополнительную роль certificate authority web enrollment. При нажатии на Add Features устанавливаются все необходимые минимальные оснастки для работы этой роли


После ознакомления с описанием роли продолжаем этап установки next


Также можно выбрать несколько дополнительных оснасток для установки после этого переходим next


В ходе установки и настройки может понадобиться перезагрузка при установке отметки сервер это сделает в автоматическом режиме


Поле установки ролей необходимо их сконфигурировать переходим в Configure Active Directory Certificate Services


Для настройки конфигурации AD CS необходима указать учетную запись с необходимым набором привилегий и перейти next


Отметкой в check box выбираем службы которые будут сконфигурированы и выбираем next


Standalone CA Характеризуются нетребовательностью к наличию домена Active Directory. Так же Standalone CA не использует шаблоны, следовательно вы не можете запрашивать сертификаты на таком CA через оснастку Certificates консоли MMC. Что означает, что вы не можете пользоваться функциями autoenrollment’а. Так же при выдаче сертификатов, Standalone CA не может автоматически публиковать сертификаты в свойства учётной записи пользователя или компьютера


Указываем тип ЦС (Specify the type of CA) – Корневой ЦС (Root CA) переходим next


Необходимо создать закрытый ключ ЦС, чтобы он мог создавать и выдавать их клиентам. Для этого выбираем «Создать новый закрытый ключ (Create a new private key) переходим next


В качестве поставщика службы шифрования выбираем один из трёх предложенных и открытого ключа  RSA 2048 бита переходим next


Укажите имя центра сертификации различающего имени Demo.wsr переходим next


Указываем необходимый «срок годности (validaty period)» корневого сертификата (в нашем случае было выбрано 5 лет) переходим next


Указываем расположение баз данных сертификатов переходим next


В окне «Подтверждения (Confirmation) сверяем введённую информацию переходим Configure


Далее появится окно результатов об успешной установке компонентов


На устройстве SRV в оснастке server manager переходим на вкладку Tools далее IIS Manager


Выбираем сервер SRV Home далее переходим server certificates


выберем – «Создать самоподписанный сертификат (Create Self-Signed Certificate)


указываем Имя сертификата (Friendly Name),  Выбираем тип веб хостинг (Web Hosting) переходим ok


Теперь необходимо привязать этот сертификат для доступа по https к веб-серверу выбираем Default Web Site далее Edit Bindings


 Добавляем (Add) — выбрать https – и выбрать самоподписанный SSL-сертификат переходим ok


Для проверки развернем иерархию Default Web Site выберем сайт CerSrv далее browse *:443 (https)


Убедимся что в internet explorer отображается сертификат


Инфраструктура веб-приложения.

Данный блок подразумевает установку и настройку доступа к веб-приложению, выполненному в формате контейнера Docker

Образ Docker (содержащий веб-приложение) расположен на ISO-образе дополнительных материалов;
В данном варианте образ приложения находиться на ISO необходимо зайти в настройки устройств WEB-L и WEB-R что бы убедиться в том что необходимое устройство подключено


2. Пакеты для установки Docker расположены на дополнительном ISO-образе;

На устройстве WEB-L в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


Docker — это платформа контейнеризации с открытым исходным кодом, с помощью которой можно автоматизировать создание приложений, их доставку и управление. Платформа позволяет быстрее тестировать и выкладывать приложения, запускать на одной машине требуемое количество контейнеров.

Для установки docker используем команду

1
apt install -y docker-ce


Для запуска и добавления в автоматическую загрузку используем команду

1
2
systemctl start docker
systemctl enable docker


После установки необходимо создать директорию /mnt/app куда будет смонтирован CDROM на котором находиться приложение командой

1
mkdir /mnt/app


монтируем CDROM в созданную директорию командой

1
mount /dev/sr1 /mnt/app


Команда docker load загрузит образ или репозиторий из архива tar расположенного на CDROM

1
docker load < /mnt/app/app.tar


Для проверки наличия контейнера для запуска используем команду

1
docker images


На устройстве WEB-R в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


Для установки docker используем команду

1
apt install -y docker-ce


Для запуска и добавления в автоматическую загрузку используем команду

1
2
systemctl start docker
systemctl enable docker


После установки необходимо создать директорию /mnt/app куда будет смонтирован CDROM на котором находиться приложение командой

1
mkdir /mnt/app
монтируем CDROM в созданную директорию командой

1
mount /dev/sr1 /mnt/app


Команда docker load загрузит образ или репозиторий из архива tar расположенного на CDROM

1
docker load < /mnt/app/app.tar


Для проверки наличия контейнера для запуска используем команду

1
docker images


3. Инструкция по работе с приложением расположена на дополнительном ISO-образе;

В данной момент инструкции к приложению нет, приложением является одностраничный сайт

4. Необходимо реализовать следующую инфраструктуру приложения

Хостинг приложения осуществляется на ВМ WEB-Ldocker run — команда, запускающая контейнер—restart=always — всегда перезапускать остановленный контейнер, если контейнер не был остановлен—name — имя контейнера для запуска-p — это связывает порт на котором будет запушен контейнер-d — Запустите контейнер в фоновом режиме и распечатайте идентификатор контейнера

После того как контейнер будет запущен в фонов режиме используем команду docker ps

1
2
docker run  --restart=always --name app  -p 8080:80 -d app
docker ps


Для проверки работа способности контейнера локально используем команду

1
curl http://localhost:8080


Для проверки работа способности контейнера по доменному имении используем команду

1
curl http://webapp1.int.demo.wsr:8080


Хостинг приложения осуществляется на ВМ WEB-R

После того как контейнер будет запущен в фонов режиме используем команду docker ps

1
2
docker run  --restart=always --name app  -p 8080:80 -d app
docker ps


Для проверки работа способности контейнера локально используем команду

1
curl http://localhost:8080


Для проверки работа способности контейнера по доменному имении используем команду

1
curl http://webapp2.int.demo.wsr:8080


Доступ к приложению осуществляется по DNS-имени www.demo.wsr

На устройстве WEB-L в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


nginx — это HTTP-сервер и обратный прокси-сервер, почтовый прокси-сервер, а также TCP/UDP прокси-сервер общего назначения для установки используем команду

1
apt install -y nginx


после установки необходимо отредактировать конфигурационный файл расположенный в директории /etc/nginx/sites-available/default

1
nano /etc/nginx/sites-available/default


Имена сервера задаются с помощью директивы server_name и определяют, в каком блоке server будет обрабатываться тот или иной запросlocation — структура в конфигурационном файле Nginx определяющая к URl и запросам какого вида будут применяться описанные правилазадаем конфигурацию для указания на на webapp1

1
2
3
4
5
6
7
8
9
10
11
12
server {
    server_name www.demo.wsr;
 location / {
  proxy_pass http://webapp1.int.demo.wsr:8080 ;
 }
}
server {
   listen 80  default_server;
  server_name _;
  return 301 http://www.demo.wsr;
 
}


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Для применения конфигурации необходимо перезагрузить сервис командой

1
systemctl reload nginx


На устройстве WEB-R в командной строке необходимо ввести команду, для добавления нового CDROM в список доступных источников APT

1
apt-cdrom add


для установки используем команду

1
apt install -y nginx


после установки необходимо отредактировать конфигурационный файл расположенный в директории /etc/nginx/sites-available/default

1
nano /etc/nginx/sites-available/default


задаем конфигурацию для указания на на webapp2

1
2
3
4
5
6
7
8
9
10
11
12
server {
    server_name www.demo.wsr;
 location / {
  proxy_pass http://webapp2.int.demo.wsr:8080 ;
 }
}
server {
   listen 80  default_server;
  server_name _;
  return 301 http://www.demo.wsr;
 
}


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Для применения конфигурации необходимо перезагрузить сервис командой

1
systemctl reload nginx


Имя должно разрешаться во “внешние” адреса ВМ управления трафиком в обоих регионах;

На устройстве RTR-L Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Для проброса портов Для настройки статического NAT необходимо отключить HTTP и HTTPS командами

1
2
3
no ip http secure-server
do wr
reload


Для настройки статического NAT необходимо использовать команду

1
2
ip nat inside source static tcp 192.168.100.100 80 4.4.4.100 80
ip nat inside source static tcp 192.168.100.100 443 4.4.4.100 443
На устройстве RTR-R Перейти в привилегированный режим EXEC можно выполнив команду

1
enable
Далее необходимо перейти в  режима глобальной конфигурации

1
configure terminal
Для проброса портов Для настройки статического NAT необходимо отключить HTTP и HTTPS командами

1
2
3
no ip http secure-server
do wr
reload


Для настройки статического NAT необходимо использовать команду

1
2
ip nat inside source static tcp 172.16.100.100 80 5.5.5.100 80
ip nat inside source static tcp 172.16.100.100 443 5.5.5.100 443


Для проверки на устройстве CLI в microsoft edge необходимо открыть страницу http://www.demo.wsr


Доступ к приложению должен быть защищен с применением технологии TLS

Протокол TLS (transport layer security) основан на протоколе SSL (Secure Sockets Layer), изначально разработанном в Netscape для повышения безопасности электронной коммерции в Интернете.

На устройстве SRV в браузере internet explorer необходимо перейти на https://localhost/CertSrvдалее more information и разрешить самоподписанный сертификат


Переходим в раздел для запроса нового сертификата


Выбираем расширенные функции запроса сертификата


Выбираем создание и отправку запроса на центр сертификации


Заполняем необходимые поля выбираем тип, способ шифрования и экспорт закрытой части ключа


Далее выбираем submit


После чего будет создан запрос на центре сертификации для выпуска сертификата переходим home


На устройстве SRV в оснастке server manager переходим на вкладку Tools далее certificate authority


Раскрываем в оснастки иерархию переходим на ожидаемые запросы далее правой кнопкой мыши all Tasks далее Issue


На устройстве SRV в браузере internet explorer необходимо перейти на https://localhost/CertSrvна странице Home выбираем просмотр сертификатов


Выбираем выпущенный сертификат


Далее устанавливаем на устройство SRV


После процесса установки будет сообщение что все прошло успешно


На устройстве SRV используем сочетание клавиш crtl + r вводим в появившемся окне mmc и enter далее file и add/remove для добавления оснастки для работы с сертификатами


Выбираем раздел Certificates далее add так как сертификат установился под пользователя необходимо выбрать раздел My user


После добавления в оснастку необходимо развернуть иерархию выбрав Personal далее Certificates. Сертификат http://www.demo.wsrнеобходимо экспортировать с закрытой частью ключа


После ознакомление с краткой информацией оснастки переходим next


Выбираем пункт экспортирование закрытой части ключа переходим next


выбираем формат экспорта переходим next


Задаем пароль на сертификат например 123 переходим далее


указываем путь куда будет сохранен сертификат в данном случае это директория SMB которая перемонтирована к WEB-L и WEB-R выбираем next


После проверки выбираем Finish


Убедимся в том что сертификат был экспортирован в директорию SMB


на устройстве WEB-L необходимо перейти в перемонтированную директорию по SMB используя команду

1
cd /opt/share


OpenSSL – это криптографическая библиотека, которая является open source реализацией двух протоколов: Secure Sockets Layer (SSL) и Transport Layer Security (TLS). Данная библиотека имеет инструменты, предназначенные для генерации приватных ключей RSA и Certificate Signing Requests (CSR-запросов)

Далее необходимо из сертификата http://www.pfxэкспортировать закрытую часть используя команду и ввести пароль 123

1
openssl pkcs12 -nodes -nocerts -in www.pfx -out www.key


и экспортировать открытаю часть используя команду и ввести пароль 123

1
openssl pkcs12 -nodes -nokeys -in www.pfx -out www.cer


Скопируем закрытую и открытою часть в директорию /etc/nginx/ используя команду

1
2
3
cp /opt/share/www.key /etc/nginx/www.key
 
cp /opt/share/www.cer /etc/nginx/www.cer


Изменим конфигурацию по умолчанию расположенная в директории /etc/nginx/snippets/snakeoil.conf

1
nano /etc/nginx/snippets/snakeoil.conf


Укажем расположение открытой и закрытой части ключа

1
2
ssl_certificate /etc/nginx/www.cer
ssl_certificate_key /etc/nginx/www.key


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Изменим конфигурацию по умолчанию расположенная в директории /etc/nginx/sites-available/default

1
nano /etc/nginx/sites-available/default


Добавим директорию и порт где находиться открытая и закрытая части ключа

1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
server {
    listen 443 ssl default_server;
    include /etc/nginx/snippets/snakeoil.conf;
 
    server_name www.demo.wsr;
 
 location / {
  proxy_pass http://webapp1.int.demo.wsr:8080 ;
 }
}
 
server {
   listen 80  default_server;
  server_name _;
  return 301 https://www.demo.wsr;
 
}


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Для применения конфигурации необходимо перезагрузить сервис командой

1
systemctl reload nginx


Для проверки на устройстве CLI в microsoft edge необходимо открыть страницу http://www.demo.wsr


на устройстве WEB-R необходимо перейти в перемонтированную директорию по SMB используя команду

1
cd /opt/share


из сертификата http://www.pfxранее экспортировали закрытую часть и открытою часть для проверки используем команду

1
 


Скопируем закрытую и открытою часть в директорию /etc/nginx/ используя команду

1
2
cp /opt/share/www.key /etc/nginx/www.key
cp /opt/share/www.cer /etc/nginx/www.cer


Изменим конфигурацию по умолчанию расположенная в директории /etc/nginx/snippets/snakeoil.conf

1
nano /etc/nginx/snippets/snakeoil.conf


Укажем расположение открытой и закрытой части ключа

1
2
ssl_certificate /etc/nginx/www.cer
ssl_certificate_key /etc/nginx/www.key


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Изменим конфигурацию по умолчанию расположенная в директории /etc/nginx/sites-available/default

1
nano /etc/nginx/sites-available/default


Добавим директорию и порт где находиться открытая и закрытая части ключа

1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
server {
    listen 443 ssl default_server;
    include /etc/nginx/snippets/snakeoil.conf;
 
    server_name www.demo.wsr;
 
 location / {
  proxy_pass http://webapp2.int.demo.wsr:8080 ;
 }
}
 
server {
   listen 80  default_server;
  server_name _;
  return 301 https://www.demo.wsr;
 
}


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Для применения конфигурации необходимо перезагрузить сервис командой

1
systemctl reload nginx


Для проверки на устройстве CLI в microsoft edge необходимо открыть страницу http://www.demo.wsr


На устройстве SRV в браузере internet explorer необходимо перейти на https://localhost/CertSrvдалее Download a CA certificate


Выбираем Base 64 формат и скачиваем root сертификат


Далее заходим в загрузки переименовываем файл в ca.cer и копируем его в директорию SMB


на устройстве CLI необходимо запустить powershell и использовать команду для того что бы скачать root сертификат


Для установки сертификата двойным кликом открываем его переходим install Certificate


Выбираем local machine далее next


Указываем директорию куда будет установлен сертификат Browse далее в Trusted Root Certification далее ok


Проверяем выбранные поля далее Finish


Для проверки на устройстве CLI в microsoft edge необходимо открыть страницу http://www.demo.wsr


5. Необходимо обеспечить отказоустойчивость приложения;

На устройстве WEB-L Изменим конфигурацию по умолчанию расположенная в директории /etc/nginx/sites-available/default

1
nano /etc/nginx/sites-available/default


Добавим upstream и изменение location

1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
upstream backend {
 server 192.168.100.100:8080 fail_timeout=25;
 server 172.16.100.100:8080 fail_timeout=25;
}
  
server {
    listen 443 ssl default_server;
    include /etc/nginx/snippets/snakeoil.conf;
 
    server_name www.demo.wsr;
 
 location / {
  proxy_pass http://backend ;
 }
}
 
server {
   listen 80  default_server;
  server_name _;
  return 301 https://www.demo.wsr;
 
}


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Для применения конфигурации необходимо перезагрузить сервис командой

1
systemctl reload nginx


На устройстве WEB-R Изменим конфигурацию по умолчанию расположенная в директории /etc/nginx/sites-available/default

1
nano /etc/nginx/sites-available/default


Добавим upstream и изменение location

1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
upstream backend {
 server 192.168.100.100:8080 fail_timeout=25;
 server 172.16.100.100:8080 fail_timeout=25;
}
  
server {
    listen 443 ssl default_server;
    include /etc/nginx/snippets/snakeoil.conf;
 
    server_name www.demo.wsr;
 
 location / {
  proxy_pass http://backend ;
 }
}
 
server {
   listen 80  default_server;
  server_name _;
  return 301 https://www.demo.wsr;
 
}


Для сохранения конфигурационного файла в текстовом редакторе nano необходимо нажать сочетание славишь ctrl + X и затем Y


Для применения конфигурации необходимо перезагрузить сервис командой

1
systemctl reload nginx






curl запросов
Команда curl
Перед тем как перейти к описанию того как может использоваться команда curl linux, давайте разберем саму утилиту и ее основные опции, которые нам понадобятся. Синтаксис утилиты очень прост:

$ curl опции ссылка

Теперь рассмотрим основные опции:

-# - отображать простой прогресс-бар во время загрузки;
-0 - использовать протокол http 1.0;
-1 - использовать протокол шифрования tlsv1;
-2 - использовать sslv2;
-3 - использовать sslv3;
-4 - использовать ipv4;
-6 - использовать ipv6;
-A - указать свой USER_AGENT;
-b - сохранить Cookie в файл;
-c - отправить Cookie на сервер из файла;
-C - продолжить загрузку файла с места разрыва или указанного смещения;
-m - максимальное время ожидания ответа от сервера;
-d - отправить данные методом POST;
-D - сохранить заголовки, возвращенные сервером в файл;
-e - задать поле Referer-uri, указывает с какого сайта пришел пользователь;
-E - использовать внешний сертификат SSL;
-f - не выводить сообщения об ошибках;
-F - отправить данные в виде формы;
-G - если эта опция включена, то все данные, указанные в опции -d будут передаваться методом GET;
-H - передать заголовки на сервер;
-I - получать только HTTP заголовок, а все содержимое страницы игнорировать;
-j - прочитать и отправить cookie из файла;
-J - удалить заголовок из запроса;
-L - принимать и обрабатывать перенаправления;
-s - максимальное количество перенаправлений с помощью Location;
-o - выводить контент страницы в файл;
-O - сохранять контент в файл с именем страницы или файла на сервере;
-p - использовать прокси;
--proto - указать протокол, который нужно использовать;
-R -  сохранять время последнего изменения удаленного файла;
-s - выводить минимум информации об ошибках;
-S - выводить сообщения об ошибках;
-T - загрузить файл на сервер;
-v - максимально подробный вывод;
-y - минимальная скорость загрузки;
-Y - максимальная скорость загрузки;
-z - скачать файл, только если он был модифицирован позже указанного времени;
-V - вывести версию.
Это далеко не все параметры curl linux, но здесь перечислено все основное, что вам придется использовать.

Как пользоваться curl?
Мы рассмотрели все, что касается теории работы с утилитой curl, теперь пришло время перейти к практике, и рассмотреть примеры команды curl.

Загрузка файлов
Самая частая задача - это загрузка файлов linux. Скачать файл очень просто. Для этого достаточно передать утилите в параметрах имя файла или html страницы:

curl https://raw.githubusercontent.com/curl/curl/master/README.md

Но тут вас ждет одна неожиданность, все содержимое файла будет отправлено на стандартный вывод. Чтобы записать его в какой-либо файл используйте:

curl -o readme.txt https://raw.githubusercontent.com/curl/curl/master/README.md

А если вы хотите, чтобы полученный файл назывался так же, как и файл на сервере, используйте опцию -O:

curl -O https://raw.githubusercontent.com/curl/curl/master/README.md

Если загрузка была неожиданно прервана, вы можете ее возобновить:

curl -# -C - -O https://cdn.kernel.org/pub/linux/kernel/v4.x/testing/linux-4.11-rc7.tar.xz

 

Если нужно, одной командой можно скачать несколько файлов:

curl -O https://raw.githubusercontent.com/curl/curl/master/README.md -O https://raw.githubusercontent.com/curl/curl/master/README



Еще одна вещь, которая может быть полезной администратору - это загрузка файла, только если он был изменен:

curl -z 21-Dec-17 https://raw.githubusercontent.com/curl/curl/master/README.md -O https://raw.githubusercontent.com/curl/curl/master/README

Данная команда скачает файл, только если он был изменен после 21 декабря 2017.

Ограничение скорости
Вы можете ограничить скорость загрузки до необходимого предела, чтобы не перегружать сеть с помощью опции -Y:

curl --limit-rate 50K -O https://cdn.kernel.org/pub/linux/kernel/v4.x/testing/linux-4.11-rc7.tar.xz

Здесь нужно указать количество килобайт в секунду, которые можно загружать. Также вы можете разорвать соединение если скорости недостаточно, для этого используйте опцию -Y:

curl -Y 100 -O https://raw.githubusercontent.com/curl/curl/master/README.md

Передача файлов
Загрузка файлов, это достаточно просто, но утилита позволяет выполнять и другие действия, например, отправку файлов на ftp сервер. Для этого существует опция -T:

curl -T login.txt ftp://speedtest.tele2.net/upload/

Или проверим отправку файла по HTTP, для этого существует специальный сервис:

curl -T ~/login.txt http://posttestserver.com/post.php

В ответе утилита сообщит где вы можете найти загруженный файл.

Отправка данных POST
Вы можете отправлять не только файлы, но и любые данные методом POST. Напомню, что этот метод используется для отправки данных различных форм. Для отправки такого запроса используйте опцию -d. Для тестирования будем пользоваться тем же сервисом:

curl -d "field1=val&fileld2=val1"http://posttestserver.com/post.php

Если вас не устраивает такой вариант отправки, вы можете сделать вид, что отправили форму. Для этого есть опция -F:

curl -F "password=@pass;type=text/plain" http://posttestserver.com/post.php

Здесь мы передаем формой поле password, с типом обычный текст, точно так же вы можете передать несколько параметров.

Передача и прием куки
Куки или Cookie используются сайтами для хранения некой информации на стороне пользователя. Это может быть необходимо, например, для аутентификации. Вы можете принимать и передавать Cookie с помощью curl. Чтобы сохранить полученные Cookie в файл используйте опцию -c:

curl -c cookie.txt http://posttestserver.com/post.php

Затем можно отправить cookie curl обратно:

curl -b cookie.txt http://posttestserver.com/post.php

Передача и анализ заголовков
Не всегда нам обязательно нужно содержимое страницы. Иногда могут быть интересны только заголовки. Чтобы вывести только их есть опция -I:

curl -I https://losst.pro

А опция -H позволяет отправить нужный заголовок или несколько на сервер, например, можно передать заголовок If-Modified-Since чтобы страница возвращалась только если она была изменена:

curl -I --header 'If-Modified-Since: Mon, 26 Dec 2016 18:13:12 GMT' https://losst.pro

Аутентификация curl
Если на сервере требуется аутентификация одного из распространенных типов, например, HTTP Basic или FTP, то curl очень просто может справиться с такой задачей. Для указания данных аутентификации просто укажите их через двоеточие в опции -u:

curl -u ftpuser:ftppass -T - ftp://ftp.testserver.com/myfile_1.txt

Точно так же будет выполняться аутентификация на серверах HTTP.

Использование прокси
Если вам нужно использовать прокси сервер для загрузки файлов, то это тоже очень просто. Достаточно задать адрес прокси сервера в опции -x:

curl -x proxysever.test.com:3128http://google.co.in
ldap
Шаг 2: Установите OpenLDAP Server на Ubuntu 22.04|20.04|18.04
Когда закончите, установите пакеты LDAP, выполнив приведенные ниже команды:

sudo apt update
sudo apt -y install slapd ldap-utils
Во время установки вам будет предложено установить LDAP admin password, введите желаемый пароль, затем нажмите <OK>


Подтвердите пароль и продолжите установку, выбрав <ok> с помощью клавиши TAB.


Вы можете подтвердить, что ваша установка прошла успешно, используя slapcat выведите содержимое базы данных SLAPD.

$ sudo slapcat
dn: dc=example,dc=com
objectClass: top
objectClass: dcObject
objectClass: organization
o: example.com
dc: example
structuralObjectClass: organization
entryUUID: 3096cde2-64b5-103c-836e-1d0b0995a781
creatorsName: cn=admin,dc=example,dc=com
createTimestamp: 20220510135944Z
entryCSN: 20220510135944.468673Z#000000#000#000000
modifiersName: cn=admin,dc=example,dc=com
modifyTimestamp: 20220510135944Z
Шаг 3: Добавьте базовое имя пользователя для пользователей и групп
Следующим шагом будет добавление базового DN для пользователей и групп. Создайте файл с именем basedn.ldif и приведенным ниже содержимым:

$ vim basedn.ldif
dn: ou=people,dc=example,dc=com
objectClass: organizationalUnit
ou: people

dn: ou=groups,dc=example,dc=com
objectClass: organizationalUnit
ou: groups
Замените example and com на нужные компоненты вашего домена.

Теперь добавьте файл, выполнив команду:

$ ldapadd -x -D cn=admin,dc=example,dc=com -W -f basedn.ldif
Enter LDAP Password:
adding new entry "ou=people,dc=example,dc=com"
adding new entry "ou=groups,dc=example,dc=com"
Шаг 4: Добавьте учетные записи пользователей и группы
Сгенерируйте пароль для учетной записи пользователя для добавления.

$ sudo slappasswd
New password:
Re-enter new password:
{SSHA}Zn4/E5f+Ork7WZF/alrpMuHHGufC3x0k
Создайте ldif-файл для добавления пользователей.

$ vim ldapusers.ldif
dn: uid=computingforgeeks,ou=people,dc=example,dc=com
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
cn: computingforgeeks
sn: Wiz
userPassword: {SSHA}Zn4/E5f+Ork7WZF/alrpMuHHGufC3x0k
loginShell: /bin/bash
uidNumber: 2000
gidNumber: 2000
homeDirectory: /home/computingforgeeks
Замените computingforgeeks именем пользователя для добавления
dc=пример,dc=com с вашими правильными значениями домена.
& sn с вашими значениями имени пользователя
{SSHA}Zn4/E5f +Ork7WZF /alrpMuHHGufC3x0k с вашим хэшированным паролем
Когда закончите редактировать, добавьте учетную запись, выполнив.

$ ldapadd -x -D cn=admin,dc=example,dc=com -W -f ldapusers.ldif
Enter LDAP Password:
adding new entry "uid=computingforgeeks,ou=people,dc=example,dc=com"
Сделайте то же самое с группой. Создайте ldif-файл:

$ vim ldapgroups.ldif
dn: cn=computingforgeeks,ou=groups,dc=example,dc=com
objectClass: posixGroup
cn: computingforgeeks
gidNumber: 2000
memberUid: computingforgeeks
Добавить группу:

$ ldapadd -x -D cn=admin,dc=example,dc=com -W -f ldapgroups.ldif
Enter LDAP Password:
adding new entry "cn=computingforgeeks,ou=groups,dc=example,dc=com"
Вы можете объединить их в один файл.

Шаг 5: Установите LDAP Account Manager – Рекомендуется
Я заметил, что phpLDAPadmin плохо работает с PHP 7.2 +. Я рекомендую вам вместо этого использовать LDAP Account Manager. Следуйте нашему руководству ниже, чтобы установить и настроить LDAP Account Manager.

Установите и настройте LDAP Account Manager на Ubuntu
В руководстве также показано, как добавить учетные записи пользователей и группы на ваш сервер LDAP.

Шаг 6: Настройте свой Ubuntu 22.04|20.04|18.04 в качестве LDAP-клиента
Последний шаг - настроить системы в вашей сети для аутентификации на сервере LDAP, который мы только что настроили:

Настройте LDAP-клиент на Ubuntu
Шаг 7: Защита LDAP-сервера / клиента
Защитите свой LDAP-сервер и доступ с LDAP-клиента с помощью TLS / SSL:

Защищенный LDAP-сервер с помощью SSL / TLS на Ubuntu
Завершение
Спасибо, что воспользовались нашим руководством по установке и настройке OpenLDAP server на вашем Ubuntu. Я рекомендую установить и использовать диспетчер учетных записей LDAP для администрирования вашего LDAP-сервера через веб-интерфейс.


apt install slapd ldap-utils
при установке в файле /etc/ldap/ldap.conf  поменять uri и ldap:// на нужный

после этого создать файл *.ldif по пути /etc/ldap/schema и с помощью команды
ldapadd -x(без sasl аутентификации) -D(домен) "cn=admin,dc=*,dc=*" -W -f *.ldif

для создания организационных единиц

минимальный конфиг для создания пользователей

расширенный конфиг для создания пользователей


команда для проверки всей конфигурации slapcat
команда для проверки отдельных единиц

справка по ошибкам
https://pro-ldap.ru/tr/admin24/appendix-common-errors.html

включение аутентификации на пользователях
cat /etc/nsswitch.conf
passwd: files ldap
group: files ldap
shadow: files ldap
gshadow: files

ldap менеджер учеток
Установите менеджер учетных записей LDAP на Ubuntu 22.04|20.04|18.04|16.04

Менеджер учетных записей LDAP (LAM) - это веб-интерфейс для управления записями (например, пользователями, группами, настройками DHCP), хранящимися в каталоге LDAP. Инструмент диспетчера учетных записей LDAP был разработан, чтобы максимально упростить управление LDAP для пользователя.

Я упрощаю администрирование записей LDAP, абстрагируясь от технических деталей LDAP и позволяя администраторам и пользователям без технического образования управлять сервером LDAP. При необходимости опытные пользователи могут напрямую редактировать записи LDAP через встроенный браузер LDAP.

Особенности диспетчера учетных записей LDAP
Управляет ключами Unix, Samba 3/4, Kolab 3, Kopano, DHCP, SSH, группой имен и многим другим
Поддерживает двухфакторную аутентификацию
Поддержка профилей создания учетных записей
Загрузка CSV-файла
Автоматическое создание / удаление домашних каталогов
настройка квот файловой системы
Вывод PDF для всех учетных записей
схема и браузер LDAP
управляет несколькими серверами с различными конфигурациями
Вы можете разблокировать дополнительные функции с помощью LDAP Account Manager pro edition, такие как:


Пользователи могут редактировать свои собственные данные (например, пароль, адрес, номера телефонов, ...)
Использует сброс своих собственных паролей
Поддержка самостоятельной регистрации пользователей
Поддержка пользовательской схемы LDAP
Unix, Samba 3/4, Kopano, …
Поддерживает несколько профилей самообслуживания (например, для разных серверов LDAP и / или вариантов использования)
Зависимости диспетчера учетных записей LDAP
Диспетчер учетных записей LDAP имеет ряд зависимостей, а именно:

Сервер OpenLDAP: Установите и настройте OpenLDAP на Ubuntu
Веб-сервер PHP и Apache
Учетная запись пользователя с привилегиями sudo
Вот шаги по установке и настройке диспетчера учетных записей LDAP в Ubuntu 22.04|20.04|18.04|16.04 LTS-сервер.

Шаг 1: Установите сервер OpenLDAP
У вас должен быть установлен и запущен сервер LDAP, вы можете воспользоваться нашим руководством по настройке на новой Ubuntu 22.04|20.04|18.04|16.04 LTS сервер:

Установите и настройте OpenLDAP на Ubuntu
Шаг 2: Установите веб-сервер Apache и PHP
Установите PHP и веб-сервер Apache, выполнив приведенные ниже команды на вашем терминале

sudo apt -y install apache2 php php-cgi libapache2-mod-php php-mbstring php-common php-pear
Затем включите php-cgi расширение PHP.

Используя подстановочный знак;

sudo a2enconf php*-cgi
sudo systemctl reload apache2
Или с помощью специальной команды:

Ubuntu 22.04:

sudo a2enconf php8.1-cgi
sudo systemctl reload apache2
Ubuntu 20.04:

sudo a2enconf php7.4-cgi
sudo systemctl reload apache2
Ubuntu 18.04:

sudo a2enconf php7.2-cgi
sudo systemctl reload apache2
Ubuntu 16.04:

sudo a2enconf php7.0-cgi
sudo systemctl reload apache2
Шаг 3: Установите диспетчер учетных записей LDAP
Пакет диспетчера учетных записей LDAP доступен в репозиториях Ubuntu, установите его с помощью команды:

sudo apt -y install ldap-account-manager
По завершении установки рекомендуется ограничить доступ к веб-панели мониторинга, разрешив использовать только доверенные локальные подсети. Однако это необязательно, и вы можете пропустить это.

sudo vim /etc/apache2/conf-enabled/ldap-account-manager.conf
Отредактируйте строку 12, чтобы прокомментировать строку Require all granted и добавить подсети, разрешенные для доступа к интерфейсу администрирования LDAP Account Manager.

#Require all granted
Require ip 127.0.0.1 192.168.10.0/24 192.168.18.0/24
Перезапустите веб-сервер apache после внесения изменений:

sudo systemctl restart apache2
Шаг 4: Настройте диспетчер учетных записей LDAP
Получите доступ к веб-интерфейсу диспетчера учетных записей LDAP из сети доверенного компьютера на:

http://(server’s hostname or IP address)/lam
Будет показана форма для входа в менеджер учетных записей LDAP. Нам нужно настроить наш профиль сервера LDAP, нажав на[LAM configuration] в правом верхнем углу.

Затем нажмите на,Edit server profiles


Это запросит у вас имя профиля LAM пароль:



Пароль по умолчанию - lam
Первое, что нужно изменить, это то, Profile Password что находится в конце страницы общих настроек .


Далее необходимо установить LDAP Server address и Tree suffix. Мой выглядит так, как показано ниже, вам нужно использовать компоненты вашего домена, указанные в имени хоста сервера.


Установите вход в панель мониторинга, указав учетную запись пользователя администратора и компоненты домена в разделе “Настройки безопасности” .


Перейдите на страницу “Типы учетных записей” и установите для активных типов учетных записей суффикс LDAP и атрибуты списка.


Вы также можете включить другие доступные типы учетных записей, которые вы хотите использовать. Пользовательские и групповые модули можно включать и отключать на странице“Модули” .

Когда закончите с настройками, нажмите кнопку Сохранить в нижней части страницы.

Шаг 5: Добавьте учетные записи пользователей и группы с помощью диспетчера учетных записей LDAP
Войдите под этой учетной записьюadmin в панель управления LAM, чтобы начать управлять учетными записями пользователей и группами.


Вы будете использовать ссылки "Пользователи "и "Группы " для управления учетными записями пользователей и группами.

Добавьте группу пользователей
Вам нужно добавить группу пользователей перед фактической учетной записью пользователя. Нажмите на Группы> Новая группа


Дайте группе имя, необязательный идентификатор группы и описание.


Сделайте то же самое, чтобы добавить другие группы.

Добавьте учетные записи пользователей
Как только у вас будут добавлены группы для учетных записей пользователей, нажмите Пользователи> Новый пользователь , чтобы добавить новую учетную запись пользователя на ваш сервер LDAP. У вас есть три раздела для управления пользователями:

Личный – содержит личную информацию пользователя, такую как имя, фамилия, адрес электронной почты, телефон, отдел, адрес и т.д.

Unix: В этом разделе вы устанавливаете имя пользователя, общее имя, UID-номер (необязательно), комментарий пользователя, основную группу пользователей и дополнительные группы, домашний каталог и оболочку входа по умолчанию.

Тень: В этом разделе вы добавляете расширение теневой учетной записи, что связано со старением / истечением срока действия пароля.

У вас может быть больше разделов в зависимости от модулей , включенных для управления пользователями и группами.

После добавления учетных записей пользователей последним шагом является настройка ваших серверов Linux и приложений для аутентификации на вашем сервере LDAP. Смотрите наше руководство о том, как настроить Ubuntu 22.04|20.04|18.04|16.04 LTS в качестве LDAP-клиента для этого.
вернуться к apache studio directory

kubernetes
Обновите aptиндекс пакета и установите пакеты, необходимые для использования репозитория Kubernetes apt:

sudo apt-get update

sudo apt-get install -y ca-certificates curl
Если вы используете Debian 9 (stretch) или более раннюю версию, вам также потребуется установитьapt-transport-https:

sudo apt-get install -y apt-transport-https


Загрузите общедоступный ключ подписи Google Cloud:
sudo curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

Добавьте репозиторий Kubernetes apt:
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

Обновите aptиндекс пакета с помощью нового репозитория и установите kubectl:
sudo apt-get update

sudo apt-get install -y kubectl

решение ошибки с ключами

curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -

echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
docker-compose
https://hub.docker.com/_/mediawiki - docker-compose.yml
для сборки yml файла команда docker-compose build

NodeExporteк prometeus grafana

настройка согласно задания
пересмотреть по конфигурации
порты в докере
3000:3000
слева порт самой машины
справа порт контейнера
media-wiki
7 пункт не прорешен все остальное решается в самом файле который берется с docker.hub
docker-registry
Настройка и запуск Docker Registry
Docker Registry — это инструмент, хранения и обмена docker образами. Его образ можно скачать и запустить с официального репозитория Docker Hub.

Для запуска docker registry выполните в консоли:

# sudo docker run -d -p 5000:5000 --restart=always --name registry registry:2

Эта команда скачает registry и запустит контейнер на порту 5000 (ключ -p 5000:5000). Данный контейнер будет запускаться автоматически при старте docker.

В минимальной конфигурации мы запустили наш частный аналог docker hub. Давайте попробуем отправить свой образ в только что созданный репозиторий. Для это нам нужно протегировать образ. Возьмем для примера, образ, который мы создали в прошлой статье. Либо можете использовать любой другой образ:

# sudo docker tag microsevice_v1 <host ip>:5000/microsevice_v1

Данной командой мы добавили тег для локального образа. Формат тега:

<hostname | ip >:post/<image name>:<tag>

В моем случае это будет:

# sudo docker tag microsevice_v1 192.168.0.19:5000/microsevice_v1

Можно указать localhost:5000, но тогда вы не сможем выполнить pull данного образа с другого компьютера в сети. Также можно указать hostname, если у вас настроен DNS.

После выполните:

# sudo docker push 192.168.0.19:5000/microsevice_v1

Вывод консоли будет примерно таким:

The push refers to repository [192.168.0.19:5000/microsevice_v1]
Get https://192.168.0.19:5000/v2/: http: server gave HTTP response to HTTPS client
Утилита docker ожидает, что registry работает по защищенному соединению HTTPS. Можно разрешить подключение по незащищенному протоколу, для в конфигурационный файл демона docker нужно добавить следующую строку:

# sudo nano /etc/docker/daemon.json

{
"insecure-registries": ["192.168.0.19:5000"]
}
После чего перезапустить демон docker.

# sudo systemctl restart docker

Еще раз попробуйте команду push.

Вывод будет примерно такой:



The push refers to repository [192.168.0.19:5000/microsevice_v1]
e547e1a2483f: Layer already exists
efda78b6f2ad: Layer already exists
8c41c444e844: Layer already exists
042a80566ff4: Layer already exists
151a914ab9a4: Layer already exists
be340e26398b: Layer already exists
d7994f7c0aa0: Layer already exists
0bd71a837902: Layer already exists
13cb14c2acd3: Layer already exists
latest: digest: sha256:0ae5b4cf9e0f6891b6b83ef62beeb6a97247fc9bd1057fba91476fea834b781e size: 2203
Ваш образ должен успешно отправился в ваш приватный репозиторий.

Чтобы посмотреть список images в репозитории можно воспользоваться curl или браузером:

# curl -X GET http://192.168.0.19:5000/v2/_catalog


Теперь можно создать Dockefile и указать в качестве источника образа свой приватный репозиторий.

Dockefile

FROM 192.168.0.19:5000/microsevice_v1
CMD ["uvicorn", "--host", "0.0.0.0", "main:app"]
Запустите сборку образа:

# sudo docker build .



Sending build context to Docker daemon 794.6kB
Step 1/2 : FROM 192.168.0.19:5000/microsevice_v1
---> de81735cd2c9
Step 2/2 : CMD ["uvicorn", "--host", "0.0.0.0", "main:app"]
---> Running in 126edf70fb82
Removing intermediate container 126edf70fb82
---> 906f2df00d0c
Successfully built 906f2df00d0c
Видно, что образ подтянулся с локального репозитория и собрался контейнер.

То же самое можно сделать командой run, если вам нужно просто запустить контейнер, а не использовать его за основу.

sudo docker run -d 192.168.0.19:5000/microsevice_v1

Данную команду можно выполнить на любом другом компьютере в сети, и тем самым мы обеспечили простой механизм переноса образов docker внутри сети. Docker Registry позволяет автоматизировать процесс deploy сервисов и их обмен при командной разработке.

Настройка авторизации в Docker Registry
Docker Registry поддерживает несколько способов аутентфикации, но в этой статье мы рассмотрим простую basic authentication.

Для начала установите утилиту, для генерации пароля:

sudo apt install apache2-utils

Затем создайте папку, для хранения настроек и Dockerfile.

# mkdir -p ~/docker-registry/auth/# cd ~/docker-registry/auth/

Для генерации пароля выполните:

# htpasswd -Bc registry.password docker-adm

Где docker-adm — это имя пользователя (его можно изменить на произвольное), ключ -B это принудительное шифрование пароля bcrypt, -с путь и имя создаваемого файла.



New password:
Re-type new password:
Adding password for user docker-adm
Утилита запросит пароль и подтверждение, после чего создаст файл

registry.password.

Перейдите на уровень выше в папку ~/docker-registry/

# cd ~/docker-registry/

Запустите docker-registry со следующими переменными окружения:

docker run -d -p 5000:5000 -e REGISTRY_AUTH=htpasswd -e REGISTRY_AUTH_HTPASSWD_REALM=Registry -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/registry.password -e REGISTRY_STORAGE_FILESYSTEM_ROOTDIRECTORY=/data -v "$PWD/data:/data" -v "$PWD/auth:/auth" --name registry registry:2

Ключи -e позволяют передать в контейнер переменные окружения(environment).

REGISTRY_AUTH — тип аутентификации, htpasswd;
REGISTRY_AUTH_HTPASSWD_REALM — это заголовок realm описанный в RFC 7235;
REGISTRY_AUTH_HTPASSWD_PATH — это путь до файла registry.password внутри контейнера, его не следует менять;
REGISTRY_STORAGE_FILESYSTEM_ROOTDIRECTORY — это путь до директории, где будет храниться образы внутри контейнера.
Команда длинная, лучше ее скопировать в текстовый файл, или в sh скрипт. Пример скрипта, можно взять здесь

Ключ -v нужен, чтобы пробросить директории с host машины в контейнер. В нашем примере указано, что директория «$PWD/auth» — где $PWD это текущая директория, из которой производится запуск + auth будет смонтирована в контейнер в директорию /auth. Аналогично и второй ключ -v.

Вывод в консоль после выполнения команды:


Создался конейнер с hash: 6f430176572dfeca6e81be763206ea35d182facacd27ca9e732822b523d8906a

Если вы видите следующую ошибку:



docker: Error response from daemon: Conflict. The container name "/registry" is already in use by container "6f430176572dfeca6e81be763206ea35d182facacd27ca9e732822b523d8906a". You have to remove (or rename) that container to be able to reuse that name.
See 'docker run --help'.
В данном случае контейнер уже запущен (мы его запустили в самом начале работы с docker registry). Можно остановить его и удалить контейнер командой:

# sudo docker stop registry && sudo docker rm registry

Теперь можно повторить запуск registry.

Для проверки аутентфикации, перейдите по адресу:

http://192.168.0.19:5000/v2/_catalog
В окне нужно указать логин и пароль, который был указан при генерации файла registry.password.


Если вы выполните аутентификацию, в окне браузера отобразится содержимое docker репозитория.


Наш репозиторий пуст.

Если выполнить команду push , указанную ранее, в registry появится один образ:


Но важно, теперь чтобы выполнить push в репозиторий, нужно сначала авторизоваться через консоль. Для этого нужно выполнить:

# sudo docker login login http://192.168.0.19:5000# sudo docker push 192.168.0.19:5000/microsevice_v1



Username: docker-adm
Password:
WARNING! Your password will be stored unencrypted in /home/devel/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store
Login Succeeded
В дальнейшем, мы будем использовать registry для автоматизации развертывания сервисов в kubernetes.
docker file
FROM название контейнера(HelloRosatom) если не запускается dockerfile ошибка в названии или в командах
FROM

Инструкция FROMзадает образ контейнера, который будет применяться при создании нового образа. Например, при использовании инструкции FROM mcr.microsoft.com/windows/servercoreполученный образ является производным и зависимым от базового образа ОС Windows Server Core. Если указанный образ отсутствует в системе, где выполняется процесс сборки Docker, подсистема Docker попытается скачать его из общедоступного или частного реестра образов.

Формат инструкции FROM выглядит следующим образом:

DockerfileКопировать
FROM <image>


Ниже приведен пример команды FROM.

Чтобы загрузить Windows Server Core версии ltsc2019 из Реестра контейнеров (Майкрософт):

Копировать
FROM mcr.microsoft.com/windows/servercore:ltsc2019

WORKDIR /home(указание корневой или рабочей директории)
run

Инструкция RUN задает команды, которые следует выполнить и поместить в новый образ контейнера. Эти команды могут включать такие элементы, как установка программного обеспечения, создание файлов и папок, а также создание конфигурации среды.

Инструкция RUN выглядит следующим образом:

Dockerfile
# exec form


RUN ["<executable>", "<param 1>", "<param 2>"]


# shell form


RUN <command>

Различие между формой исполняемого файла (exec form) и формой оболочки (shell form) заключается в способе выполнения инструкции RUN. При использовании формы исполняемого файла указанная программа запускается явным образом.

Ниже приведен пример формы исполняемого файла.


FROM mcr.microsoft.com/windows/servercore:ltsc2019


RUN ["powershell", "New-Item", "c:/test"]


Полученный образ выполняет команду powershell New-Item c:/test:

Dockerfile
docker history doc-exe-method


IMAGE CREATED CREATED BY SIZE COMMENT
b3452b13e472 2 minutes ago powershell New-Item c:/test 30.76 MB


В отличие от предыдущего примера здесь та же операция выполняется с использованием формы оболочки:

Копировать
FROM mcr.microsoft.com/windows/servercore:ltsc2019


RUN powershell New-Item c:\test


Полученный образ содержит инструкцию RUN cmd /S /C powershell New-Item c:\test.

DockerfileКопировать
docker history doc-shell-method


IMAGE CREATED CREATED BY SIZE COMMENT
062a543374fc 19 seconds ago cmd /S /C powershell New-Item c:\test 30.76 MB
COPY

Инструкция COPYкопирует файлы и каталоги в файловую систему контейнера. Эти файлы и каталоги должны иметь путь, являющийся относительным для Dockerfile.

Формат инструкции COPYвыглядит следующим образом:

DockerfileКопировать
COPY <source> <destination>

Если источник или назначение содержит пробел, заключите путь в квадратные скобки и двойные кавычки, как показано в следующем примере:

DockerfileКопировать
COPY ["<source>", "<destination>"]

Рекомендации по использованию инструкции COPY с Windows

В Windows для путей назначения необходимо использовать символы косой черты. Например, здесь показаны допустимые инструкции COPY.

DockerfileКопировать
COPY test1.txt /temp/
COPY test1.txt c:/temp/

При этом следующий формат с обратными косыми чертами работать не будет:

DockerfileКопировать
COPY test1.txt c:\temp\


Примеры использования инструкции COPY с Windows

В следующем примере содержимое исходного каталога добавляется в каталог с именем sqlliteв образе контейнера.

DockerfileКопировать
COPY source /sqlite/

В следующем примере все файлы, начинающиеся с config, добавляют в каталог c:\tempобраза контейнера.

DockerfileКопировать
COPY config* c:/temp/
WORKDIR

Инструкция WORKDIRзадает рабочий каталог для других инструкций Dockerfile, например RUNи CMD, а также рабочий каталог для запущенных экземпляров образа контейнера.

Формат инструкции WORKDIRвыглядит следующим образом:

DockerfileКопировать
WORKDIR <path to working directory>

Рекомендации по использованию инструкции WORKDIR с Windows

Если в Windows рабочий каталог содержит обратную косую черту, ее следует экранировать.

DockerfileКопировать
WORKDIR c:\\windows

Примеры

DockerfileКопировать
WORKDIR c:\\Apache24\\bin
CMD

Инструкция CMDзадает команду по умолчанию, выполняемую при развертывании экземпляра образа контейнера. Например, если в контейнере будет размещен веб-сервер NGINX, CMDможет включать инструкции для запуска этого веб-сервера, например с помощью команды nginx.exe. Если в файле Dockerfile указано несколько инструкций CMD, вычисляется только последняя из них.

Формат инструкции CMDвыглядит следующим образом:

DockerfileКопировать
# exec form


CMD ["<executable", "<param>"]


# shell form


CMD <command>

Рекомендации по использованию инструкции CMD с Windows

В Windows для путей к файлам, указанным в инструкции CMD, следует использовать символы косой черты или экранировать символы обратной косой черты \\. Допустимы следующие инструкции CMD:

DockerfileКопировать
# exec form


CMD ["c:\\Apache24\\bin\\httpd.exe", "-w"]


# shell form


CMD c:\\Apache24\\bin\\httpd.exe -w

Однако следующий формат без соответствующих косых черт работать не будет:

DockerfileКопировать
CMD c:\Apache24\bin\httpd.exe -w
Сценарии PowerShell

В некоторых случаях удобно скопировать крипт в контейнеры, используемые при создании образа, и затем запустить его из контейнера.
Примечание

Это ограничивает возможности кэширования слоев образов, а также ухудшает удобочитаемость файла Dockerfile.

Этот пример копирует сценарий с компьютера сборки в контейнер с помощью инструкции ADD. Затем этот сценарий выполняется с помощью инструкции RUN.

Копировать
FROM mcr.microsoft.com/windows/servercore:ltsc2019
ADD script.ps1 /windows/temp/script.ps1
RUN powershell.exe -executionpolicy bypass c:\windows\temp\script.ps1
Команда Docker build

После создания файла Dockerfile и сохранения его на диск можно запустить docker buildдля создания нового образа. Команда docker buildпринимает несколько необязательных параметров и путь к файлу Dockerfile. Полную документацию по команде Docker build, включая список всех параметров сборки, см. всправочнике по сборке.

Формат команды docker buildвыглядит следующим образом:

DockerfileКопировать
docker build [OPTIONS] PATH


Например, следующая команда создает образ с именем "iis".

DockerfileКопировать
docker build -t iis .


При инициации процесса сборки в выходных данных указывается состояние и выводятся все возникающие ошибки.

DockerfileКопировать
C:\> docker build -t iis .


Sending build context to Docker daemon 2.048 kB
Step 1 : FROM mcr.microsoft.com/windows/servercore:ltsc2019
---> 6801d964fda5


Step 2 : RUN dism /online /enable-feature /all /featurename:iis-webserver /NoRestart
---> Running in ae8759fb47db


Deployment Image Servicing and Management tool
Version: 10.0.10586.0


Image Version: 10.0.10586.0


Enabling feature(s)
The operation completed successfully.


---> 4cd675d35444
Removing intermediate container ae8759fb47db


Step 3 : RUN echo "Hello World - Dockerfile" > c:\inetpub\wwwroot\index.html
---> Running in 9a26b8bcaa3a
---> e2aafdfbe392
Removing intermediate container 9a26b8bcaa3a


Successfully built e2aafdfbe392


В результате создается новый образ контейнера, который в этом примере носит имя "iis".

DockerfileКопировать
docker images


REPOSITORY TAG IMAGE ID CREATED VIRTUAL SIZE
iis latest e2aafdfbe392 About a minute ago 207.8 MB
windowsservercore latest 6801d964fda5 4 months ago 0 B
Команды Dockerfile
Dockerfile – это скрипт, который содержит в себе инструкции, которые будут использоваться для создания образа Docker с помощью команды «docker build».Для создания Dockerfile нужно знать некоторый его команды.

FROM
Инструкция FROM инициализирует новый этап сборки и должна располагаться в верхней части Dockerfile.

LABEL
С помощью этой инструкции вы можете добавить дополнительную информацию о вашем образе Docker, такую как версия, описание и т.д.

RUN
Эта инструкция используется для выполнения команды в процессе сборки образа Docker. Вы можете установить дополнительные пакеты, необходимые для ваших образов Docker.

ADD
Инструкция ADD используется для копирования файлов, каталогов или файлов из удаленных ресурсов в ваши образы Docker. Кроме того, вы можете настроить права на файл по умолчанию.

ENV
Инструкция ENV используется для определения переменной среды, которая может использоваться на этапе сборки.

CMD
CMD используется для определения команды по умолчанию для выполнения при запуске контейнера. Dockerfile должен содержать только одну инструкцию CMD, и, если имеется несколько CMD, будет выполняться последняя инструкция CMD.

EXPOSE
Эта инструкция используется для предоставления порта контейнера на определенных сетевых портах во время выполнения. По умолчанию используется протокол TCP, но вы можете указать, будет ли он TCP или UDP.

ARG
Инструкция ARG используется для определения переменной, которую пользователь может передать сборщику образа. Вы можете использовать эту инструкцию в docker ‘build command’ во время сборки, используя опцию ‘–build-arg variable = value’. Также вы можете использовать несколько ARG в Dockerfile.

ENTRYPOINT
Инструкция ENTRYPOINT используется для определения первой команды и команды по умолчанию, которая будет выполняться при запуске контейнера. Определите команду для запуска приложения с помощью инструкции ENTRYPOINT.

WORKDIR
Инструкция WORKDIR используется для определения рабочего каталога по умолчанию вашего образа Docker. Инструкции RUN, CMD, ENTRYPOINT и ADD следуют за инструкцией WORKDIR. Вы можете добавить несколько инструкций WORKDIR в ваш Dockerfile. Если она не существует, она будет создана автоматически.

USER
Инструкция USER используется для определения пользователя или gid по умолчанию при запуске образа. RUN, CMD и ENTRYPOINT следуют за инструкциям USER в Dockerfile.

VOLUME
Инструкция VOLUME используется для создания точки монтирования между контейнером и базовым хостом.

https://learn.microsoft.com/ru-ru/virtualization/windowscontainers/manage-docker/manage-windows-dockerfile
https://docs.docker.com/engine/reference/builder/

Docker
docker run - скачивает и устанавливает контейнер с ос(создает)
docker info - выводит всю информацию по докеру, все контейнеры
docker images - список образов(самих контейнеров)
docker ps - таблица с основной информацией
docker start,stop,kill - команды для работы с СОЗДАННЫМ контейнером




static route
route add подсеть mask маска шлюз
/etc/sysctl.conf
На устройстве ISP пересылка трафика между интерфейсами В операционной системе Linux включается командой net.ipv4.ip_forward=1
sysctl — утилита, предназначенная для управления параметрами ядра на лету
Пакеты Linux
chrony - сервер времени
network-manager - для графической настройки сетевых устройств (nmtui)

центр сертификации
https://learn.microsoft.com/ru-ru/windows/win32/seccertenroll/about-certification-authorities - центры сертификации
https://learn.microsoft.com/ru-ru/windows/win32/seccrypto/certificate-services?source=recommendations - службы сертификатов
https://learn.microsoft.com/ru-ru/windows/win32/seccertenroll/public-key-infrastructure?source=recommendations - инфраструктура открытых ключей
https://learn.microsoft.com/ru-ru/windows/win32/seccrypto/certificates-and-public-keys - сертификаты и открытые ключи(как работает)
https://learn.microsoft.com/ru-ru/windows/win32/seccrypto/certification-authority-renewal - продление сертификатов, номера сообщений

iptables
https://www.opennet.ru/docs/RUS/iptables/#DNATTARGET

icmp


не достижимость


astra
ALD linux похожее на adds
Различия между hosts nsswitch и resolv.conf
https://server-fault.ru/questions/25919/razlichie-mezhdu-etc-hosts-i-etc-resolv-conf
Бауманка
Pvcreate наносит разметку на разделы pvremove
 vgcreate lvcreate
Vgextend vg1 /dev/sde расширение
Pvmove перемещение
Vgreduce сокращение или сжатие раздела
Lvresize уаеличение или уменьшение раздела 
-r изменение размера раздела и файловой системы
Настройка сети
    sconfig
RAID 1
mdadm --create --verbose /dev/md0
--level=1 --raid-devices=2 /dev/sdb1 /dev/sdc1

Логи
$template LOGS,"путь"
crit.*    ?LOGS
gre тунель linux
ip link add grelan type gretap local <IP1> remote <IP2> 
ip link set grelan up 
iptables -I INPUT -p gre -s <IP2> -j ACCEPT
dhcpv6
subnet6 2001:470:1f1b:5b3::/64 {
range6 2001:470:1f1b:5b3::50 2001:470:1f1b:5b3::ffff;
range6 2001:470:1f1b:5b3::/64 temporary;
option dhcp6.name-servers 2001:470:1f1a:5b3::2;
option dhcp6.domain-search "patrikx3.tk";
}

dns вторичная зона
# Поддерживаемая зона
zone "unix.ck.ua" {
# Тип, тут указываем, что мы вторичный DNS-сервер для нашей зоны.
type slave;
# Файл Slave-зоны, указываем месторасположение.
file "slave/unix.ck.ua.slave";
# Сервер, который выступает в роли - Master. В нашем случае, указываем IP нашего Master-сервера.
masters { 82.207.87.36;};
};
ftp
chroot_local_user=YES
chroot_local_enable=YES
chroot_list_file=/etc/vsftpd/chroot_list
userlist_enable=YES
user_config_dir=/etc/vsftpd/
userlist_file=/etc/vsftpd/user_list
userlist_deny-NO
dhcp+dns
dnssec-keygen -a HMAC-MD5 -b 128 -r /dev/urandom -n USER DHCP_UPDATER
открываем через cat ключ private затем копируем
vi /.../named.conf
key DHCP_UPDATER{
    algoritm HMAC-MD5.SIG-ALG.REG.INT;
    secret "ключ";
    };
в зонах добавляем строчку allow-update {key DHCP-UPDATE;};
в конфиге dhcp:
ddns-updates on;
ddns-update-style interim;
update-static-leases on;
key DHCP-UPDATE {
    algoritm HMAC-MD5;
    secret 'ключ'
    }
zone  LAB.LOCAL {
    primary ip;
    key DHCP-UPDATE;
}
обратная по аналогии


radius linux
/etc/raddb/client.conf
nat linux
iptables -A FORWARD -s 10.8.0.0/24 -j ACCEPT 
iptables -A FORWARD -m state —state RELATED,ESTABLISHED -j ACCEPT 
iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o enp2s0 -j MASQUERADE
yum
http://www.ekzorchik.ru/2013/09/cycling-dvd-image-as-a-repository/
http://linux-notes.org/kak-sozdat-yum-repozitorij-iz-cd-rom-diska-v-redhat-centos-fedora/
yum --disablerepo=\* --enablerepo=c7-media install ...
nmtui

BR3
en
conf t
hostname HQ1
ip domain-name wsr2018.ru
username wsr2018 privilege 15 secret cisco
aaa new-model
aaa authentication login default local
line vty 0 15
login authentication default
transport input ssh
privilege level 15
exit
line console 0
login authentication default
privilege level 0
exit
enable password wsr
service password-encryption
ipv6 unicast-routing
int loopback 101
ip add 192.168.33.254 255.255.255.0
exit
int loopback 102
ip add 192.168.34.254 255.255.255.0
exit
int loopback 103
ip add 3.3.3.3 255.255.255.255
ipv6 add dead:beef::3/128
exit
int g0/0
no sh
pppoe enable
pppoe-client dial-pool-number 1
mtu 1532
exit
int Dialer1
ip add negotiated
encapsulation ppp
dialer pool 1
ppp pap sent-username cisco password cisco
exit
router eigrp 2018
network 192.168.33.0 255.255.255.0
network 20.18.64.8 255.255.255.248
network 192.168.34.0 255.255.255.0
exit
int loopback 103
ipv6 ospf 1 area 0
exit
int tunnel 100
ipv6 add 2018::2/64
tunnel source Dialer1
tunnel destination 20.18.64.2
ipv6 ospf 1 area 0
exit
user1 privilege 2 secret cisco 
privilege exec level 2 reload 
privilege exec level 2 erase startup-config 
privilege exec all level 2 debug 
privilege exec all level 2 no debug 
privilege exec all level 2 undebug

HQ1
en
conf t
hostname HQ1
ip domain-name wsr2018.ru
username wsr2018 privilege 15 secret cisco
aaa new-model
aaa authentication login default local
line vty 0 15
login authentication default
transport input ssh
privilege level 15
exit
line console 0
login authentication default
privilege level 0
exit
enable password wsr
service password-encryption
ipv6 unicast-routing
int loopback 101
ip add 11.11.11.11 255.255.255.255
ipv6 add dead:beef::1/128
exit
ipv6 dhcp pool POOL
address prefix 2018:218A:4021::/64
link-address 2018:218A:4021::1/64
domain-name wsr2018.ru
int g0/0
no sh
ip add 172.16.138.254 255.255.255.0
ipv6 add 2018:218A:4021::1/64
ipv6 dhcp server POOL
exit
ip ssh version 2
crypto key generate rsa modulus 1024
username ISP password cisco
int s0/3/0
encapsulation authentication chap
ppp chap password cisco
exit
#ip sla 1
#icmp-echo 8.8.8.8 source-interface g0/0
#threshold 2000
#timeout 2000
#frequency 120
#exit
router eigrp 2018
network 11.11.11.11 255.255.255.255
network 20.18.64.0 255.255.255.248
exit
int g0/0
ipv6 ospf 1 area 0
exit
int loopback 101
ipv6 ospf 1 area 0
exit
int tunnel 100
ipv6 add 2018::1/64
tunnel source s0/3/0
tunnel destination 20.18.64.14
ipv6 ospf 1 area 0
exit
ip dhcp excluded-address 172.16.138.1 172.16.138.99
ip dhcp pool DHCP
network 172.16.138.0 255.255.255.0
default-router 172.16.138.254
dns-server 8.8.8.8
exit

ISP
en
conf t
hostname ISP
ip domain-name wsr2018.ru
username wsr2018 privilege 15 secret cisco
aaa new-model
aaa authentication login default local
line vty 0 15
login authentication default
transport input ssh
privilege level 15
exit
line console 0
login authentication default
privilege level 0
exit
enable password wsr
service password-encryption
int loopback 101
ip add 33.136.0.1 255.255.0.0
exit
int loopback 100
ip add 8.8.8.8 255.255.255.255
exit
int loopback 102
ip add 161.66.0.1 255.255.0.0
exit
int g0/0
no sh
exit
int s0/3/0
ip add 20.18.64.1 255.255.255.248
no sh
clockrate 128000
exit
ip ssh version 2
crypto key generate rsa modulus 1024
username HQ1 password cisco
int s0/3/0
encapsulation authentication chap
ppp chap password cisco
exit
username cisco password cisco
bba-group pppoe global
virtual-template 1
int Virtual-Template1
ip unnumbered g0/0
peer default ip address pool PPPoE
ppp authentication pap
exit
ip local pool PPPoE 20.18.64..13 20.18.64.14
int g0/0
no sh
pppoe enable group global
mtu 1532
exit
router eigrp 2018
network 20.18.64.8 255.255.255.248
network 20.18.64.0 255.255.255.248
exit
ipsec-cisco
crypto isakmp policy 1
encr aes
hash md5
authentication pre-share
group 5
!
crypto isakmp key cisco address 20.17.5.1
!
!
!
crypto ipsec transform-set SET esp-aes esp-sha-hmac
!
crypto map MAP 100 ipsec-isakmp
set peer 20.17.5.1
set transform-set SET
match address 100

access-list 100 permit ip 20.17.5.0 0.0.0.7 20.17.5.0 0.0.0.7

interface GigabitEthernet0/0
crypto map MAP

certificate AD
http://ip-add/certsrv
